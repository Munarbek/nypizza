import 'package:flutter/widgets.dart';

class MyFlutterApp {
  MyFlutterApp._();

  static const _kFontFam = 'MyFlutterApp';
  static const String? _kFontPkg = null;

  static const IconData group = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData qr_code_1 = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_basket_1 = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData shopping_list_1 = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_back = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData user_1 = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_left = IconData(0xe879, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}