import 'package:nypizza/apiservice/apiclient.dart';
import 'package:nypizza/domain/model/category.dart';
import 'package:nypizza/domain/model/foods.dart';
import 'package:nypizza/domain/model/numberverify.dart';
import 'package:nypizza/domain/model/user.dart';

class Repository{
  final ApiClient _apiClient;
  Repository(this._apiClient);
  Future<Foods> fetchFoods(){
    return _apiClient.fetchFoods();
  }
  Future<List<Category>> getCategory(){
    return _apiClient.getCategory();
  }

  Future<Map<String,dynamic>?> fetchHomeContent(){
    return  _apiClient.fetchHomeContent();
  }


  Future<String?> sendSmsCode(String phoneNumber){
    return  _apiClient.sendSmsCode(phoneNumber);
  }
  Future<Map<String,dynamic>?> numberVerify(){
    return  _apiClient.fetchHomeContent();
  }
  Future<Map<String,dynamic>?> token(){
    return  _apiClient.fetchHomeContent();
  }

  Future<NumberVerify?> verify(String phoneNumber, String code) {
    return  _apiClient.verify(phoneNumber,code);
  }

  Future<UserModelProvider?>  getUserProfile(String access) {
    return _apiClient.getUserProfile(access);
  }

  Future<Map<String, dynamic>?> fetchAppContent(String token) async {
    return _apiClient.fetchAppContent(token);
  }




}