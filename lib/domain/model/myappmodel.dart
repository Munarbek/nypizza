import 'package:hive/hive.dart';
import 'package:nypizza/config/constants.dart';
import 'package:nypizza/domain/data/securitystorage/security_storage.dart';
import 'package:nypizza/domain/model/user.dart';

class MyAppModel{
  final _securityStorage=SecurityStorage();
  var _isAuth=false;
  bool get isAuth=>_isAuth;

  Future<void> checkAuth() async{
    final phone=await _securityStorage.readPhoneNumber();
    _isAuth= phone==null ? false:true;
  }

  Future<String?> readPhoneNumber() async {
    var readData = await _securityStorage.readPhoneNumber();
    return readData;
  }
  Future<Box<UserModelProvider>> createBox() async {
  return await Hive.openBox<UserModelProvider>(boxName);
  }


  Future<String?> readAccess() async {
    var readData = await _securityStorage.readAccessToken();
    return readData;
  }

  Future deletePhoneNumber() async {
    var readData = await _securityStorage.deletePhoneNumber();
    return readData;
  }

}