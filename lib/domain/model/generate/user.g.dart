// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../user.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserModelProviderAdapter extends TypeAdapter<UserModelProvider> {
  @override
  final int typeId = 1;

  @override
  UserModelProvider read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserModelProvider(
      id: fields[0] as int?,
      avatar: fields[1] as String?,
      firstName: fields[2] as String?,
      lastName: fields[3] as String?,
      phoneNumber: fields[4] as String?,
      status: fields[5] as Status?,
      uuid: fields[6] as String?,
      bonus: fields[7] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, UserModelProvider obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.avatar)
      ..writeByte(2)
      ..write(obj.firstName)
      ..writeByte(3)
      ..write(obj.lastName)
      ..writeByte(4)
      ..write(obj.phoneNumber)
      ..writeByte(5)
      ..write(obj.status)
      ..writeByte(6)
      ..write(obj.uuid)
      ..writeByte(7)
      ..write(obj.bonus);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserModelProviderAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
