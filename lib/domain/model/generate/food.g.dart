// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../food.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Food _$FoodFromJson(Map<String, dynamic> json) => Food(
      ID: json['id'] as int?,
      name: json['name'] as String?,
      image: json['image'] as String?,
      category: json['category'] == null
          ? null
          : Category.fromJson(json['category'] as Map<String, dynamic>),
      price: json['price'] as int?,
      desc: json['desc'] as String?,
      gram: json['gram'] as int?,
      isDeactivated: json['isDeactivated'] as bool?,
      isFavorite: json['isFavorite'] as bool?,
    );

Map<String, dynamic> _$FoodToJson(Food instance) => <String, dynamic>{
      'id': instance.ID,
      'name': instance.name,
      'image': instance.image,
      'category': instance.category,
      'price': instance.price,
      'desc': instance.desc,
      'gram': instance.gram,
      'isDeactivated': instance.isDeactivated,
      'isFavorite': instance.isFavorite,
    };
