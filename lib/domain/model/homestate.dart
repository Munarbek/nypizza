import 'package:flutter/material.dart';

class HomeState with ChangeNotifier{
  int _menuSelectedIndex=0;
  int _categorySelectedIndex=0;

  void setMenuSelectedIndex(int index){
    _menuSelectedIndex=index;
    notifyListeners();
  }
  void setCategorySelectedIndex(int index){
    _categorySelectedIndex=index;
    notifyListeners();
  }

  get categorySelectedIndex => _categorySelectedIndex;

  get menuSelectedIndex => _menuSelectedIndex;


}

/*  child: NestedScrollView(
              scrollBehavior: CupertinoScrollBehavior(),
              physics: const BouncingScrollPhysics(),
              controller: _scrollViewController,
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return [
                  const SliverToBoxAdapter(
                    child: TextField(
                      decoration: InputDecoration(
                          icon: Padding(
                            padding: EdgeInsets.only(left: 20.0),
                            child: Icon(Icons.search_rounded),
                          ),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintText: 'Что ты желаешь сегодня?'),
                    ),
                  ),
                  const SliverToBoxAdapter(
                      child: Padding(
                    padding: EdgeInsets.only(left: 20, bottom: 12),
                    child: Text('Акции',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w800,
                            fontFamily: 'Gliroy')),
                  )),
                  SliverToBoxAdapter(
                    child: SizedBox(
                      height: 140,
                      child: ListView.builder(
                          physics: const BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: InkWell(
                                onTap: _showBottomDialog,
                                child: Container(
                                  width: 210,
                                  height: 135,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(13),
                                  ),
                                  child: Column(
                                    children: const [
                                      SizedBox(
                                          height: 100,
                                          width: 210,
                                          child: Image(
                                            image: AssetImage(
                                                'assets/images/rectangle.png'),
                                            fit: BoxFit.cover,
                                          )),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right: 8.0, left: 8.0),
                                        child: Text(
                                            'Закажи 3 пиццы и получи одну бесплатно!',
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: 'Gliroy',
                                              fontSize: 14,
                                            )),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: 2),
                    ),
                  ),
                  CurrySliverHeader2(),
                  if (_homeState.menuSelectedIndex == 0)
                    SliverToBoxAdapter(
                      key: dataKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Text('Категории блюд',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w800,
                                    fontFamily: 'Gliroy')),
                          ),
                          TabBar(
                            onTap: (value) {
                              homeState.setCategorySelectedIndex(value);
                              tabIndex=value;
                              ll[_homeState.menuSelectedIndex]= listviewGenerate(tabIndex);

                            },
                            controller: _tabController,
                            padding: const EdgeInsets.only(
                                top: 8.0, bottom: 8, left: 20),
                            indicatorPadding: const EdgeInsets.all(0),
                            isScrollable: true,
                            physics: const BouncingScrollPhysics(),
                            unselectedLabelColor: const Color(0xFF828282),
                            labelColor: blackMain,
                            labelStyle:
                                const TextStyle(fontWeight: FontWeight.w700),
                            labelPadding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                                boxShadow: const [
                                  BoxShadow(
                                    color: Color(0x26000000),
                                    offset: Offset(0.0, 4.0), //(x,y)
                                    blurRadius: 5.0,
                                  ),
                                ],
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(color: yellowMain)),
                            tabs: List.generate(8, (index) {
                              return FoodCategoriesItem();
                            }),
                          ),
                        ],
                      ),
                    ),
                ];
              },
              body:ListView(
                controller: _scrollViewController,
                shrinkWrap: true,
                children:[ ll[_homeState.menuSelectedIndex] ],
              ),
            ),
*
* */