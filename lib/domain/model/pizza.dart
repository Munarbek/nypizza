import 'dart:math';

import 'package:flutter/cupertino.dart';

class Pizza with ChangeNotifier{
  String _id='';
  String _category;
  String _name;
  String _weight;
  int _price;
  bool _favorite;
  String _imageUrl;
  String _ingredient;
  int _countAddToCard=0;

  Pizza(this._id,this._category,this._name, this._weight, this._price, this._favorite,this._imageUrl,this._ingredient);

  bool get favorite => _favorite;

  set favorite(bool value) {
    _favorite = value;
  }

  String get category => _category;

  set category(String value) {
    _category = value;
  }

  String get ingredient => _ingredient;

  set ingredient(String value) {
    _ingredient = value;
  }

  int get price => _price;

  String get id => _id;

  set id(String value) {
    _id = value;

  }

  set price(int value) {
    _price = value;
  }

  String get imageUrl => _imageUrl;

  set imageUrl(String value) {
    _imageUrl = value;
  }

  String get weight => _weight;

  set weight(String value) {
    _weight = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  int get countAddToCard => _countAddToCard;

  set countAddToCard(int value) {
    _countAddToCard = value;
    notifyListeners();
  }


  static List<Pizza> listPizzas()=>[
    Pizza(
      '${Random().nextInt(1000)}',
      'Pizza',
      'Нью-Йорк',
      '650 гр',
      658,
      false,
      'assets/images/newyork.png',
      'соус, сыр тильзитский, говядина, курица копченная',),
    Pizza(
      '${Random().nextInt(1000)}',
      'Pizza',
      'Нью-Йорк',
      '650 гр',
      658,
      false,
      'assets/images/newyork.png',
      'соус, сыр тильзитский, говядина, курица копченная',),
    Pizza(
      '${Random().nextInt(1000)}',
      'Pizza',
      'Нью-Йорк',
      '650 гр',
      658,
      false,
      'assets/images/newyork.png',
      'соус, сыр тильзитский, говядина, курица копченная',)
  ];

}