import 'package:json_annotation/json_annotation.dart';

import 'food.dart';

part 'generate/foods.g.dart';


@JsonSerializable()
class Foods {
  int? count;
  String? next;
  String? previous;
  List<Food>? results;

  Foods({this.count, this.next, this.previous, this.results});

  factory Foods.fromJson(Map<String,dynamic>json)=>_$FoodsFromJson(json);
  Map<String ,dynamic> toJson()=>_$FoodsToJson(this);


}
