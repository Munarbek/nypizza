class Order{
  final String type;
  final String number;
  final String status;
  final String date;
  final String address;
  final String summ;

  Order({required this.type,
      required this.number,
      required this.status,
      required this.date,
      required this.address,
      required this.summ}
      );
}