import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:nypizza/domain/model/pizza.dart';
import 'package:nypizza/domain/model/status.dart';
import 'package:nypizza/domain/respository/repository.dart';
import 'package:nypizza/ui/user/providers/usermodelprovider.dart';

import 'myappmodel.dart';

part 'generate/user.g.dart';
@HiveType(typeId: 1)
class UserModelProvider {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? avatar;
  @HiveField(2)
  String? firstName;
  @HiveField(3)
  String? lastName;
  @HiveField(4)
  String? phoneNumber;
  @HiveField(5)
  Status? status;
  @HiveField(6)
  String? uuid;
  @HiveField(7)
  int? bonus;

  UserModelProvider(
      {required this.id,
        required this.avatar,
        required this.firstName,
        required this.lastName,
        required this.phoneNumber,
        required this.status,
        required this.uuid,
        required this.bonus});

  UserModelProvider.fromJson(Map<String, dynamic>? json) {
    id = json?['id'];
    avatar = json?['avatar'];
    firstName = json?['first_name'];
    lastName = json?['last_name'];
    phoneNumber = json?['phone_number'];
    status = json?['status'] != null ? Status.fromJson(json?['status']) : null;
    uuid = json?['uuid'];
    bonus = json?['bonus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['avatar'] = avatar;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['phone_number'] = phoneNumber;
    if (status != null) {
      data['status'] = status?.toJson();
    }
    data['uuid'] = uuid;
    data['bonus'] = bonus;
    return data;
  }
}




class User with ChangeNotifier {
  UserModelProvider? userModelProvider;
  final Repository _repository;
  final MyAppModel securityStorage;
  late Box<UserModelProvider> userBox;

  String? errorMessage;
  bool isLoading=true;
  String? tokenAccess;

  User(this._repository, this.securityStorage);
  Future<String?> getAccessToken() async {
    tokenAccess= await securityStorage.readAccess();
    return tokenAccess;
  }


  Future<Map<String,dynamic>?> getContentApp()  async{
    var token=await getAccessToken();
      return _repository.fetchAppContent(token!);
  }

  Future<UserModelProvider?>  getUserProfile() async{
    var token=await getAccessToken();
      //   token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjM0NTgxNDM3LCJqdGkiOiJlMjFmYTU2MmJmMDY0NTViYjljZDA3YmM0YThiZTcxMCIsInVzZXJfaWQiOjYxfQ.o4-dmz1Qm7_xY914MFTYs3_Po9lB-V4yfXCwRJqlVfk';
    return _repository.getUserProfile(token!);
  }
}
//     var token=await getAccessToken();
//     _repository.getUserProfile(token!).then((value) {
//       userModelProvider=value;
//       isLoading=false;
//       errorMessage=null;
//       notifyListeners();
//     }).catchError((error){
//       errorMessage='Что-то пошло не так.';
//       userModelProvider=null;
//       isLoading=false;
//     });
//   }
// }



