import 'package:flutter/cupertino.dart';
import 'package:nypizza/domain/model/category.dart';
import 'package:nypizza/domain/respository/repository.dart';
import 'foods.dart';

class FoodsModel with ChangeNotifier {
  final Repository _repository;

  String? errorMessage;
  bool isLoading=true;

  FoodsModel(this._repository);

  List<Category> get favoriteList => _favoriteList;
  Foods? _foods;
  List<Category> _categories = [];
  List<Category> _popularList = [];

  List<Category> get popularList => _popularList;
  List<Category> _favoriteList = [];

  List<Category> get categories => _categories;

  Foods? get getFoods => _foods;

  void fetchFoods() {
    _repository.fetchFoods().then((value) {
      _foods = value;
      notifyListeners();
    });
  }

  void fetchCategories() {
    _repository.getCategory().then((value) {
      _categories = value;
      notifyListeners();
    });
  }

  void fetchPopular() {
    _repository.getCategory().then((value) {
      _categories = value;
      notifyListeners();
    });
  }

  void fetchFavorite() {
    _repository.getCategory().then((value) {
      _categories = value;
      notifyListeners();
    });
  }

 void fetchHomeContent() async{
    _repository.fetchHomeContent().then((value) {
      _foods = value?['foods'];
      _categories = value?['category'];
      errorMessage=null;
      isLoading=false;
      notifyListeners();
    }).catchError((value){
      errorMessage = 'Что-то пошло не так.';
      isLoading=false;
      notifyListeners();
    });

  }

 void changeLoading(){
    _foods=null;
    errorMessage=null;
    isLoading=true;
    notifyListeners();
  }
}
