import 'package:json_annotation/json_annotation.dart';
import 'category.dart';
part 'generate/food.g.dart';


@JsonSerializable()
class Food {
  @JsonKey(name: 'id')
  int? ID;
  String? name;
  String? image;
  Category? category;
  int? price;
  String? desc;
  int? gram;
  bool? isDeactivated;
  bool? isFavorite;

  Food(
      {this.ID,
      this.name,
      this.image,
      this.category,
      this.price,
      this.desc,
      this.gram,
      this.isDeactivated,
      this.isFavorite});

  factory Food.fromJson(Map<String,dynamic>json)=>_$FoodFromJson(json);
  Map<String ,dynamic> toJson()=>_$FoodToJson(this);

}



