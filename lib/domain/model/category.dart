
import 'package:json_annotation/json_annotation.dart';
part 'generate/category.g.dart';
@JsonSerializable()
class Category {
  int? id;
  String? name;
  String? cover_file;
  Category({this.id,this.name,this.cover_file } );

  factory Category.fromJson(Map<String,dynamic>json)=>_$CategoryFromJson(json);
  Map<String ,dynamic> toJson()=>_$CategoryToJson(this);

}