import 'package:hive/hive.dart';

part 'generate/status.g.dart';
@HiveType(typeId: 2)
class Status {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? name;
  @HiveField(2)
  int? value;
  @HiveField(3)
  int? discount;
  @HiveField(4)
  String? picture;
  @HiveField(5)
  String? desc;
  @HiveField(6)
  bool? isActive;

  Status(
      {required this.id,
        required this.name,
        required this.value,
        required this.discount,
        required this.picture,
        required this.desc,
        required this.isActive});

  Status.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    value = json['value'];
    discount = json['discount'];
    picture = json['picture'];
    desc = json['desc'];
    isActive = json['is_active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['value'] = value;
    data['discount'] = discount;
    data['picture'] = picture;
    data['desc'] = desc;
    data['is_active'] = isActive;
    return data;
  }
}
