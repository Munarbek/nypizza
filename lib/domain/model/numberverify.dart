class NumberVerify {
 late String access;
 late String refresh;

  NumberVerify({required this.access, required this.refresh});

  NumberVerify.fromJson(Map<String, dynamic> json) {
    access = json['access'] as String;
    refresh = json['refresh'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, String>{};
    data['access'] = access;
    data['refresh'] = refresh;
    return data;
  }
}