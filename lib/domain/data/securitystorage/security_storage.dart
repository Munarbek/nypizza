import 'package:flutter_secure_storage/flutter_secure_storage.dart';

abstract class StorageKeys {
  static const accessToken = 'accessToken';
  static const refreshToken = 'refreshToken';
  static const phoneNumber = 'phoneNumber';
  static const name = 'name';
  static const url = 'urlImage';
  static const bonus = 'urlImage';
  static const status = 'urlImage';
}

class SecurityStorage {
  static const _securityStorage = FlutterSecureStorage();

  void setPhoneNumber(String phone) async =>
      await _securityStorage.write(key: StorageKeys.phoneNumber, value: phone);

  void setAccessToken(String access) async =>
      await _securityStorage.write(key: StorageKeys.accessToken, value: access);

  void setRefreshToken(String refresh) async => await _securityStorage.write(
      key: StorageKeys.refreshToken, value: refresh);



  Future<Map<String, String?>> userSecurityInformation() async {
    var number = await _securityStorage.read(key: StorageKeys.phoneNumber);
    var access = await _securityStorage.read(key: StorageKeys.accessToken);
    var refresh = await _securityStorage.read(key: StorageKeys.refreshToken);
    return {
      StorageKeys.phoneNumber: number,
      StorageKeys.accessToken: access,
      StorageKeys.refreshToken: refresh,
    };
  }

  Future<bool> outUser() async {
    await _securityStorage.delete(key: StorageKeys.phoneNumber);
    await _securityStorage.delete(key: StorageKeys.accessToken);
    await _securityStorage.delete(key: StorageKeys.refreshToken);
    return true;
  }


  Future<String?> readPhoneNumber() async {
    var readData = await _securityStorage.read(key: StorageKeys.phoneNumber);
    return readData;
  }


  Future<String?> readAccessToken() async {
    var readData = await _securityStorage.read(key: StorageKeys.accessToken);
    return readData;
  }

  Future deletePhoneNumber() async {
    var deleteData =
        await _securityStorage.delete(key: StorageKeys.phoneNumber);
    return deleteData;
  }
}
