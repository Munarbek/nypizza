import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:nypizza/domain/model/status.dart';

import 'package:nypizza/navigation/main_navigation.dart';
import 'package:nypizza/widgets/inherited/appinherited.dart';
import 'package:path_provider/path_provider.dart';

import 'domain/model/user.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDirectory = await getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  //var hive = await Hive.openBox<Map<String, dynamic>>('userBox');
  Hive.registerAdapter(UserModelProviderAdapter());
  Hive.registerAdapter(StatusAdapter());
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  State<MyApp> createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    print('Build  Main Screen ');
    /* SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: mainBlockColor,
      ),
    );*/
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return  AppInherited(child: const MyAppInherited());

  }
  @override
  void dispose() {
    Hive.close();
    super.dispose();

  }
}

class MyAppInherited extends StatelessWidget {
  const MyAppInherited({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mainNavigation = AppInherited.of(context);
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
            systemOverlayStyle: SystemUiOverlayStyle.dark
        ),
      ),
      routes: mainNavigation.mainNavigation.routes,
      initialRoute: MainNavigationRouteName.splash,
      onGenerateRoute: mainNavigation.mainNavigation.onGenerateRoute,
    );
  }
}






