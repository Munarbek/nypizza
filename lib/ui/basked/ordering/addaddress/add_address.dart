import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';

class AddAddressScreen extends StatelessWidget {
  var _streetHouse = 'Улица, дом';
  var _searchInMap = 'Найти на карте';
  var _ofis = 'Квартира/офис';
  var _podezd = 'Подъезд';
  var _etaj = 'Этаж';
  var _save = 'Сохранить';

  AddAddressScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Material(
                  color: Colors.transparent,
                  borderOnForeground: false,
                  child: InkWell(
                    enableFeedback: false,
                    borderRadius: BorderRadius.circular(40),
                    onTap: () {},
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: SizedBox(
                          height: 16,
                          width: 16,
                          child: Image(
                              image:
                                  AssetImage('assets/images/arrow_left.png'))),
                    ),
                  ),
                ),
                const Text(
                  'Вернуться в корзину',
                  style: TextStyle(
                      color: Color(0xFFB0B1B2),
                      fontSize: 18,
                      fontFamily: 'Gliroy'),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            const Text('Добавление нового адреса',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 24),
            Text(_streetHouse,
                style: const TextStyle(
                  color: Color(0xFFC6C7CA),
                  fontSize: 17,
                )),
            const SizedBox(height: 4),
            Theme(
              data: Theme.of(context).copyWith(splashColor: Colors.transparent),
              child: TextField(
                keyboardType: TextInputType.number,
                autofocus: false,
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy'),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  errorText: '*обязательное поле',
                  errorStyle: const TextStyle(
                      fontSize: 14,
                      color: redMain,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy'),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  contentPadding: const EdgeInsets.only(
                      left: 16.0, bottom: 15.0, top: 15.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 18),
            InkWell(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(_searchInMap,
                      style: const TextStyle(
                        color: redMain,
                        fontSize: 17,
                        fontFamily: 'Gliroy',
                        fontWeight: FontWeight.w400,
                      )),
                  const SizedBox(width: 4),
                  const Icon(
                    Icons.location_on,
                    color: redMain,
                  )
                ],
              ),
            ),
            const SizedBox(height: 30),
            Text(_ofis,
                style: const TextStyle(
                  color: Color(0xFFC6C7CA),
                  fontSize: 17,
                  fontFamily: 'Gliroy',
                  fontWeight: FontWeight.w400,
                )),
            const SizedBox(height: 4),
            Theme(
              data: Theme.of(context).copyWith(splashColor: Colors.transparent),
              child: TextField(
                keyboardType: TextInputType.number,
                autofocus: false,
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy'),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  errorStyle: const TextStyle(
                      fontSize: 12,
                      color: redMain,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy'),
                  contentPadding: const EdgeInsets.only(
                      left: 16.0, bottom: 15.0, top: 15.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 18),
            Text(_podezd,
                style: const TextStyle(
                  color: Color(0xFFC6C7CA),
                  fontSize: 17,
                  fontFamily: 'Gliroy',
                  fontWeight: FontWeight.w400,
                )),
            const SizedBox(height: 4),
            Theme(
              data: Theme.of(context).copyWith(splashColor: Colors.transparent),
              child: TextField(
                keyboardType: TextInputType.number,
                autofocus: false,
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy'),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  errorStyle: const TextStyle(
                      fontSize: 13,
                      color: redMain,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy'),
                  contentPadding: const EdgeInsets.only(
                      left: 16.0, bottom: 15.0, top: 15.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 18),
            Text(_etaj,
                style: const TextStyle(
                  color: Color(0xFFC6C7CA),
                  fontSize: 17,
                  fontFamily: 'Gliroy',
                  fontWeight: FontWeight.w400,
                )),
            const SizedBox(height: 4),
            Theme(
              data: Theme.of(context).copyWith(splashColor: Colors.transparent),
              child: TextField(
                keyboardType: TextInputType.number,
                autofocus: false,
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy'),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  errorStyle: const TextStyle(
                      fontSize: 12,
                      color: redMain,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy'),
                  contentPadding: const EdgeInsets.only(
                      left: 16.0, bottom: 15.0, top: 15.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 70),

            SizedBox(
              width: double.infinity,
              height: 45,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(

                    primary: redMain,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30), // <-- Radius
                    ),
                  ),
                  onPressed: () {},
                  child: Text(_save,
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Gliroy',
                        fontSize: 15,
                      ))),
            ),
            const SizedBox(height: 48),
          ],
        ),
      ),
    );
  }
}
