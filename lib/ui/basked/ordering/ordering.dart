import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/widgets/custom_dropdown.dart';

class OrderingScreen extends StatefulWidget {
  const OrderingScreen({Key? key}) : super(key: key);

  @override
  State<OrderingScreen> createState() => _OrderingScreenState();
}

class _OrderingScreenState extends State<OrderingScreen> {
  var _menuSelectedIndex = 0;
  var _isSelectedmenu = false;
  bool enableList = false;
  late int _selectedIndex;
  String? dropdownValue = null;
  var payIndex = 0;
  var _menuPrice = 1664;
  var _totalPrice = 1664;
  var _bonus=60;
  var _delivery_price=120;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Material(
                  color: Colors.transparent,
                  borderOnForeground: false,
                  child: InkWell(
                    enableFeedback: false,
                    borderRadius: BorderRadius.circular(40),
                    onTap: () {},
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: SizedBox(
                          height: 16,
                          width: 16,
                          child: Image(
                              image:
                                  AssetImage('assets/images/arrow_left.png'))),
                    ),
                  ),
                ),
                const Text(
                  'Вернуться в корзину',
                  style: TextStyle(
                      color: Color(0xFFB0B1B2),
                      fontSize: 18,
                      fontFamily: 'Gliroy'),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),




            const Text('Оформление заказа',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(
              height: 12
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Material(
                  elevation: 10,
                  shadowColor: const Color(0x1AAFBCEE),
                  borderRadius: BorderRadius.circular(10),
                  color: backgroundItemMenu,
                  child: InkWell(
                    splashColor: background,
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 12, horizontal: 15),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 1),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Text(
                        'Доставка',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: _menuSelectedIndex == 0
                              ? redMain
                              : const Color(0xFF828282),
                          fontFamily: 'Gliroy',
                          fontSize: 15,
                        ),
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        _isSelectedmenu=!_isSelectedmenu;
                        _menuSelectedIndex = 0;
                      });
                    },
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                Material(
                    elevation: 10,
                    shadowColor: const Color(0x1AAFBCEE),
                    borderRadius: BorderRadius.circular(10),
                    color: backgroundItemMenu,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _isSelectedmenu=!_isSelectedmenu;
                          _menuSelectedIndex = 1;
                        });
                      },
                      splashColor: background,
                      borderRadius: BorderRadius.circular(10),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 12, horizontal: 15),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.white, width: 1),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Text('Самовывоз',
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: _menuSelectedIndex == 1
                                  ? redMain
                                  : const Color(0xFF828282),
                              fontFamily: 'Gliroy',
                              fontSize: 15,
                            )),
                      ),
                    )),
              ],
            ),
            const SizedBox(height: 18),

            if(_menuSelectedIndex==0)
            deliveryWidget(_menuSelectedIndex),

            const Text('Метод оплаты',
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 12),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 16),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x0D000000),
                    offset: Offset(0.0, 4.0), //(x,y)
                    blurRadius: 14.0,
                  )
                ],
              ),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        payIndex = 0;
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 12),
                      child: Row(
                        children: [
                          Image(
                            image: payIndex == 0
                                ? AssetImage('assets/images/select.png')
                                : AssetImage('assets/images/unselect.png'),
                            width: 15,
                            height: 15,
                          ),
                          const SizedBox(width: 16),
                          const Image(
                              image: AssetImage('assets/images/image_spot.png'),
                              width: 32,
                              height: 32),
                          const SizedBox(width: 16),
                          const Text('Наличными',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: 'Gliroy')),
                        ],
                      ),
                    ),
                  ),
                  const Divider(height: 1, color: Color(0xFFE3000F)),
                  InkWell(
                    onTap: () {
                      setState(() {
                        payIndex = 1;
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 12),
                      child: Row(
                        children: [
                          Image(
                            image: payIndex == 1
                                ? AssetImage('assets/images/select.png')
                                : AssetImage('assets/images/unselect.png'),
                            width: 15,
                            height: 15,
                          ),
                          const SizedBox(width: 16),
                          const Image(
                              image: AssetImage('assets/images/image_spot.png'),
                              width: 32,
                              height: 32),
                          const SizedBox(width: 16),
                          const Text('Картой ',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: 'Gliroy')),
                          const Text('(KICB, Optima, Demir)',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: 'Gliroy'))
                        ],
                      ),
                    ),
                  ),
                  const Divider(height: 1, color: Color(0xFFE3000F)),
                  InkWell(
                    onTap: () {
                      setState(() {
                        payIndex = 2;
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 12),
                      child: Row(
                        children: [
                          Image(
                            image: payIndex == 2
                                ? AssetImage('assets/images/select.png')
                                : AssetImage('assets/images/unselect.png'),
                            width: 15,
                            height: 15,
                          ),
                          const SizedBox(width: 16),
                          const Image(
                              image: AssetImage('assets/images/image_spot.png'),
                              width: 32,
                              height: 32),
                          const SizedBox(width: 16),
                          const Text('Элсом',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: 'Gliroy')),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 24),
            Text('Итого за блюда: $_menuPrice сом',
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 22),
            const Text('Бонусы для списания',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 8),
            Theme(
              data: Theme.of(context).copyWith(splashColor: Colors.transparent),
              child: TextField(
                focusNode: FocusNode(),
                keyboardType: TextInputType.number,
                autofocus: false,
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy'),
                decoration: InputDecoration(
                  filled: true,
                  hintTextDirection:TextDirection.rtl ,
                  fillColor: Colors.white,
                  contentPadding: const EdgeInsets.only(
                      left: 16.0, bottom: 15.0, top: 15.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFD9D0E3)),
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 6),
            const Text('(не более 50% от суммы за блюда)', style: TextStyle(
              color:  Color(0xFFC6C7CA),
              fontSize: 12,
            )),

            const SizedBox(height: 18),
             Text('Бонусы к начислению: $_bonus',
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 18),
            Row(
              children: [
                Text('Стоимость доставки: $_delivery_price сом',
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Gliroy')),
                const SizedBox(width: 6),
                const InkWell(
                  child: Image(
                    height: 18,
                    width: 18,
                    image: AssetImage('assets/images/info.png'),
                  ),
                )
              ],
            ),
            const SizedBox(height: 18),
             Text('Итого по счету: $_totalPrice сом',
                style: const TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),


            const SizedBox(height: 20),
            const Text('(не более 50% от суммы за блюда)', style: TextStyle(
              color:  Color(0xFFC6C7CA),
              fontSize: 12,
            )),
            const SizedBox(height: 6),
            Theme(
              data: Theme.of(context).copyWith(splashColor: Colors.transparent),
              child: TextField(
                keyboardType: TextInputType.number,
                autofocus: false,
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy'),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  contentPadding: const EdgeInsets.only(
                      left: 16.0, bottom: 15.0, top: 15.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Color(0xFFD9D0E3) ,width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Color(0xFFD9D0E3),width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 24),


            SizedBox(
              width: double.infinity,
              height: 45,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(

                    primary: redMain,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30), // <-- Radius
                    ),
                  ),
                  onPressed: () {},
                  child: Text(  'Оплатить $_totalPrice сом',
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Gliroy',
                        fontSize: 15,
                      ))),
            ),


             const SizedBox(height: 48),
          ],
        ),
      ),
    );
  }

  Widget deliveryWidget(int menuSelectedIndex)=>Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: const [
                BoxShadow(
                  color: Color(0x0D000000),
                  offset: Offset(0.0, 3.0), //(x,y)
                  blurRadius: 4.0,
                )
              ],
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                isExpanded: true,
                value: dropdownValue,
                borderRadius: BorderRadius.circular(12),
                icon: const Icon(
                  Icons.expand_more_outlined,
                  color: Colors.black,
                ),
                iconSize: 28,
                elevation: 4,
                dropdownColor: Colors.white,
                hint: const Text('Выберите адрес из списка',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w800,
                        fontFamily: 'Gliroy')),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: <String>[
                  'ул. Боконбаева 113, кв. 28',
                  'ул. Тоголок-Молдо 5/1, кв. 56'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w800,
                            fontFamily: 'Gliroy')),
                  );
                }).toList(),
              ),
            ),
          ),
          const SizedBox(height: 18),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 14),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: const [
                BoxShadow(
                  color: Color(0x0D000000),
                  offset: Offset(0.0, 4.0), //(x,y)
                  blurRadius: 5.0,
                )
              ],
            ),
            child: const Text(
              ' +   Добавить новый адрес',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: redMain,
                fontFamily: 'Gliroy',
                fontSize: 16,
              ),
            ),
          ),
          const SizedBox(height: 32),
        ],
      );








}
