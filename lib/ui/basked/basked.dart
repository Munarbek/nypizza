import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/domain/model/category.dart';
import 'package:nypizza/domain/model/pizza.dart';
import 'package:nypizza/widgets/food_categories_item.dart';
import 'package:nypizza/widgets/item_pizza.dart';

class BaskedScreen extends StatelessWidget {
  BaskedScreen({Key? key}) : super(key: key);
  final scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Container(color: background, child: TabMenu(scrollController));
  }
}

class TabMenu extends StatefulWidget {
  ScrollController _scrollController;
   TabMenu(this._scrollController, {Key? key}) : super(key: key);

  @override
  _TabMenuState createState() => _TabMenuState();
}

class _TabMenuState extends State<TabMenu> {
  final list = [
    Category(
        id: 1,
        name: 'Food category 1',
        cover_file: 'assets/images/imagePizzaMenu.png'),
    Category(
        id: 2,
        name: 'Food category 2',
        cover_file: 'assets/images/imagePizzaMenu.png'),
    Category(
        id: 3,
        name: 'Food category 3',
        cover_file: 'assets/images/imagePizzaMenu.png'),
    Category(
        id: 4,
        name: 'Food category 4',
        cover_file: 'assets/images/imagePizzaMenu.png'),
    Category(
        id: 5,
        name: 'Food category 5',
        cover_file: 'assets/images/imagePizzaMenu.png'),
    Category(
        id: 6,
        name: 'Food category 6',
        cover_file: 'assets/images/imagePizzaMenu.png'),
    Category(
        id: 7,
        name: 'Food category 7',
        cover_file: 'assets/images/imagePizzaMenu.png'),
    Category(
        id: 8,
        name: 'Food category 18',
        cover_file: 'assets/images/imagePizzaMenu.png'),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: list.length,
      child: Column(
        children: <Widget>[
          TabBar(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            indicatorPadding: const EdgeInsets.all(0),
            isScrollable: true,
            physics: const BouncingScrollPhysics(),
            unselectedLabelColor: const Color(0xFF828282),
            labelColor: blackMain,
            labelStyle: const TextStyle(
              fontWeight: FontWeight.w700
            ),
            labelPadding: const EdgeInsets.symmetric(horizontal: 8.0),
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x26000000),
                    offset: Offset(0.0, 4.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: yellowMain)),
            tabs: List.generate(list.length, (index) {
              return FoodCategoriesItem( Category(
                  id: 1,
                  name: 'Food category 1',
                  cover_file: 'assets/images/imagePizzaMenu.png'));
            }),
          ),
          Expanded(
            child: TabBarView(
            children: List.generate(list.length, (index) {
              return Container(
                child: ListView.builder(
                 controller: widget._scrollController,
                  itemCount: 0,
                    itemBuilder: (context, index) {
                      return ItemPizzaHome(
                        pizza: null,
                      );
                    },
                ),
              );
            }),
            ),
          )
        ],
      ),
    );
  }
}




class BubbleTabIndicator extends Decoration {
  final double indicatorHeight;
  final Color indicatorColor;
  final double indicatorRadius;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry insets;
  final TabBarIndicatorSize tabBarIndicatorSize;

  const BubbleTabIndicator({
    this.indicatorHeight: 20.0,
    this.indicatorColor: Colors.greenAccent,
    this.indicatorRadius: 100.0,
    this.tabBarIndicatorSize = TabBarIndicatorSize.label,
    this.padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
    this.insets: const EdgeInsets.symmetric(horizontal: 5.0),
  });

  @override
  Decoration? lerpFrom(Decoration? a, double t) {
    if (a is BubbleTabIndicator) {
      return new BubbleTabIndicator(
        padding: EdgeInsetsGeometry.lerp(a.padding, padding, t)!,
        insets: EdgeInsetsGeometry.lerp(a.insets, insets, t)!,
      );
    }
    return super.lerpFrom(a, t);
  }

  @override
  Decoration? lerpTo(Decoration? b, double t) {
    if (b is BubbleTabIndicator) {
      return new BubbleTabIndicator(
        padding: EdgeInsetsGeometry.lerp(padding, b.padding, t)!,
        insets: EdgeInsetsGeometry.lerp(insets, b.insets, t)!,
      );
    }
    return super.lerpTo(b, t);
  }

  @override
  _BubblePainter createBoxPainter([VoidCallback? onChanged]) {
    return new _BubblePainter(this, onChanged);
  }
}

class _BubblePainter extends BoxPainter {
  _BubblePainter(this.decoration, VoidCallback? onChanged) : super(onChanged);

  final BubbleTabIndicator decoration;

  double get indicatorHeight => decoration.indicatorHeight;
  Color get indicatorColor => decoration.indicatorColor;
  double get indicatorRadius => decoration.indicatorRadius;
  EdgeInsetsGeometry get padding => decoration.padding;
  EdgeInsetsGeometry get insets => decoration.insets;
  TabBarIndicatorSize get tabBarIndicatorSize => decoration.tabBarIndicatorSize;

  Rect _indicatorRectFor(Rect rect, TextDirection textDirection) {
    Rect indicator = padding.resolve(textDirection).inflateRect(rect);

    if (tabBarIndicatorSize == TabBarIndicatorSize.tab) {
      indicator = insets.resolve(textDirection).deflateRect(rect);
    }

    return new Rect.fromLTWH(
      indicator.left,
      indicator.top,
      indicator.width,
      indicator.height,
    );
  }

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration.size != null);
    final Rect rect = Offset(
        offset.dx, (configuration.size!.height / 2) - indicatorHeight / 2) &
    Size(configuration.size!.width, indicatorHeight);
    final TextDirection textDirection = configuration.textDirection!;
    final Rect indicator = _indicatorRectFor(rect, textDirection);
    final Paint paint = Paint();
    paint.color = indicatorColor;
    paint.style = PaintingStyle.fill;
    canvas.drawRRect(
        RRect.fromRectAndRadius(indicator, Radius.circular(indicatorRadius)),
        paint);
  }
}

