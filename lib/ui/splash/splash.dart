import 'package:flutter/material.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:nypizza/apiservice/apiclient.dart';
import 'package:nypizza/domain/model/myappmodel.dart';
import 'package:nypizza/domain/respository/repository.dart';
import 'package:nypizza/ui/auth/authorization.dart';
import 'package:nypizza/ui/auth/provider/authmodelprovider.dart';
import 'package:nypizza/widgets/inherited/appinherited.dart';
import 'package:page_transition/page_transition.dart';


import '../../app.dart';

class MySplashScreen extends StatefulWidget {
  const MySplashScreen({Key? key}) : super(key: key);

  @override
  State<MySplashScreen> createState() => _MySplashScreenState();
}

class _MySplashScreenState extends State<MySplashScreen> {
  final myAppModel = MyAppModel();
  String? phoneNumber;
  Future<String?>? phoneNumberFuture;

  @override
  void initState() {
    //phoneNumberFuture=getPhoneNumber();
    //phoneNumber= await myAppModel.readPhoneNumber();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('Build Splash');
    return AnimatedSplashScreen(
      duration: 5000,
      splash: Padding(
        padding: const EdgeInsets.only(top: 250),
        child: Column(
          children: [
            SizedBox(child: SvgPicture.asset('assets/images/Logo.svg')),
            const Text('Попробуйте кусочек Нью-Йорка'),
          ],
        ),
      ),
      nextScreen: const AuthScreen(),
      splashTransition: SplashTransition.fadeTransition,
      imageProvider: const AssetImage('assets/images/launchlogo.png'),
    );
  }

  Future<String?> getPhoneNumber() async {
    final n = await myAppModel.readPhoneNumber();
    return n;
  }
}

enum _splashType { simpleSplash, backgroundScreenReturn }
enum SplashTransition {
  slideTransition,
  scaleTransition,
  rotationTransition,
  sizeTransition,
  fadeTransition,
  decoratedBoxTransition
}

class AnimatedSplashScreen extends StatefulWidget {
  /// Type of page transition
  final PageTransitionType transitionType;

  /// Type of splash transition
  final SplashTransition splashTransition;

  /// Only required case use [AnimatedSplashScreen.withScreenFunction]
  /// here you pass your function that need called before to jump to next screen
  final Future Function()? function;

  /// Custom animation to icon of splash
  final Animatable? customAnimation;

  /// Background color
  final Color backgroundColor;

  /// Only required in default construct, here you pass your widget that will be
  /// browsed
  final Widget? nextScreen;

  /// Type of AnimatedSplashScreen
  final _splashType type;

  /// If icon in splash need stay inside [Center] widget
  final bool centered;

  /// If you want to implement the navigation to the next page yourself.
  /// By default is [false], the widget will call Navigator.of(_context).pushReplacement()
  /// using PageTransition with [transictionType] after [duration] to [nextScreen]
  final bool disableNavigation;

  /// It can be string for [Image.asserts], normal [Widget] or you can user tags
  /// to choose which one you image type, for example:
  /// * '[n]www.my-site.com/my-image.png' to [Image.network]
  final dynamic splash;

  /// Time in milliseconds after splash animation to jump to next screen
  /// Default is [milliseconds: 2500], minimum is [milliseconds: 100]
  final int duration;

  /// Curve of splash animation
  final Curve curve;

  /// Splash animation duration, default is [milliseconds: 800]
  final Duration? animationDuration;

  /// Icon in splash screen size
  final double? splashIconSize;

  final ImageProvider? imageProvider;

  factory AnimatedSplashScreen(
      {Curve curve = Curves.easeInCirc,
      Future Function()? function,
      int duration = 2500,
      required dynamic splash,
      required Widget nextScreen,
      Color backgroundColor = Colors.white,
      Animatable? customTween,
      bool centered = true,
      bool disableNavigation = false,
      SplashTransition? splashTransition,
      PageTransitionType? pageTransitionType,
      Duration? animationDuration,
      double? splashIconSize,
      ImageProvider? imageProvider}) {
    return AnimatedSplashScreen._internal(
      backgroundColor: backgroundColor,
      animationDuration: animationDuration,
      transitionType: pageTransitionType ?? PageTransitionType.bottomToTop,
      splashTransition: splashTransition ?? SplashTransition.fadeTransition,
      splashIconSize: splashIconSize,
      customAnimation: customTween,
      function: function,
      duration: duration,
      centered: centered,
      disableNavigation: disableNavigation,
      splash: splash,
      type: _splashType.simpleSplash,
      nextScreen: nextScreen,
      curve: curve,
      imageProvider: imageProvider,
    );
  }

  const AnimatedSplashScreen._internal(
      {required this.animationDuration,
      required this.splashTransition,
      required this.customAnimation,
      required this.backgroundColor,
      required this.transitionType,
      required this.splashIconSize,
      required this.nextScreen,
      required this.function,
      required this.duration,
      required this.centered,
      required this.disableNavigation,
      required this.splash,
      required this.curve,
      required this.type,
      required this.imageProvider})
      : assert(duration != null, 'Duration cannot be null'),
        assert(transitionType != null, 'TransitionType cannot be null'),
        assert(splashTransition != null, 'SplashTransition cannot be null'),
        assert(curve != null, 'Curve cannot be null');

  @override
  _AnimatedSplashScreenState createState() => _AnimatedSplashScreenState();
}

class _AnimatedSplashScreenState extends State<AnimatedSplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  static late BuildContext _context;
  late Animation _animation;
  String? phoneNumber;
  Future<String?>? phoneNumberFuture;
  final myAppModel = MyAppModel();

  AnimatedSplashScreen get w => widget;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
        duration: w.animationDuration ?? const Duration(milliseconds: 800),
        vsync: this);

    Animatable animation = w.customAnimation ??
        () {
          switch (w.splashTransition) {
            case SplashTransition.slideTransition:
              return Tween<Offset>(
                end: Offset.zero,
                begin: const Offset(1, 0),
              );

            case SplashTransition.decoratedBoxTransition:
              return DecorationTween(
                  end: const BoxDecoration(color: Colors.black87),
                  begin: const BoxDecoration(color: Colors.redAccent));

            default:
              return Tween(begin: 0.0, end: 1.0);
          }
        }() as Animatable<dynamic>;

    _animation = animation
        .animate(CurvedAnimation(parent: _animationController, curve: w.curve));
    _animationController.forward().then((value) => doTransition());
  }

  /// call function case needed and then jump to next screen
  doTransition() async {
    if (w.type == _splashType.backgroundScreenReturn) {
      navigator(await w.function!());
    } else if (!w.disableNavigation) {
      navigator(w.nextScreen);
    }
  }

  @override
  void dispose() {
    _animationController.reset();
    _animationController.dispose();
    super.dispose();
  }

  navigator(screen) {
    myAppModel.readPhoneNumber().then((phoneNumber) {
      myAppModel.createBox().then((value) {
        try {
          print('number $phoneNumber');
          Navigator.of(_context).pushReplacement(PageTransition(
              type: w.transitionType,
              child: phoneNumber != null
                  ? InitApp(
                      phoneNumber: phoneNumber,
                    )
                  : const AuthScreen()));
        } catch (msg) {
          print('AnimatedSplashScreen -> '
              'error in jump to next screen, probably '
              'this run is in hot reload: $msg');
        }
      });
    });

    /* Future.delayed(Duration(milliseconds: w.duration < 100 ? 100 : w.duration))
        .then((_) {
      try {
        Navigator.of(_context).pushReplacement(
            PageTransition(type: w.transitionType, child: screen));
      } catch (msg) {
        print('AnimatedSplashScreen -> '
            'error in jump to next screen, probably '
            'this run is in hot reload: $msg');
      }
    });*/
  }

  /// Return icon of splash screen
  Widget getSplash() {
    final size =
        w.splashIconSize ?? MediaQuery.of(context).size.shortestSide * 0.2;

    Widget main({required Widget child}) =>
        w.centered ? Center(child: child) : child;

    return getTransition(
      child: main(
        child: Padding(
          padding: const EdgeInsets.only(top: 190),
          child: Column(
            children: [
              SizedBox(
                  width: 106,
                  height: 92,
                  child: SvgPicture.asset('assets/images/Logo.svg')),
              const SizedBox(height: 8),
              const Text('Попробуйте кусочек Нью-Йорка'),
            ],
          ),
        ),
      ),
    );
  }

  /// return transtion
  Widget getTransition({required Widget child}) {
    switch (w.splashTransition) {
      case SplashTransition.slideTransition:
        return SlideTransition(
            position: (_animation as Animation<Offset>), child: child);
        break;

      case SplashTransition.scaleTransition:
        return ScaleTransition(
            scale: (_animation as Animation<double>), child: child);
        break;

      case SplashTransition.rotationTransition:
        return RotationTransition(
            turns: (_animation as Animation<double>), child: child);
        break;

      case SplashTransition.sizeTransition:
        return SizeTransition(
            sizeFactor: (_animation as Animation<double>), child: child);
        break;

      case SplashTransition.fadeTransition:
        return FadeTransition(
            opacity: (_animation as Animation<double>), child: child);
        break;

      case SplashTransition.decoratedBoxTransition:
        return DecoratedBoxTransition(
            decoration: (_animation as Animation<Decoration>), child: child);
        break;

      default:
        return FadeTransition(
            opacity: (_animation as Animation<double>), child: child);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    //return Scaffold( backgroundColor: w.backgroundColor, body: getSplash());
    return Scaffold(
      body: Container(
        child: getSplash(),
        decoration: BoxDecoration(
            color: w.backgroundColor,
            image: DecorationImage(image: w.imageProvider!)),
      ),
    );
  }
}
