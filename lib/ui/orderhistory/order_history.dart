import 'dart:math';

import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/domain/model/order.dart';
import 'package:nypizza/ui/orderhistory/detail/order_detail.dart';

class OrderHistoryScreen extends StatefulWidget {
  final _historyOrders = 'История заказов';
  final _emptyOrders = 'Здесь будут отображаться все ваши заказы.Чтобы сделать первый, перейдите в раздел “Меню”.';

  const OrderHistoryScreen({Key? key}) : super(key: key);

  @override
  State<OrderHistoryScreen> createState() => _OrderHistoryScreenState();
}

class _OrderHistoryScreenState extends State<OrderHistoryScreen> {
  var _isSelectedOrderDetail = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: AnimatedSwitcher(
            duration: const Duration(milliseconds: 400),
          child: _isSelectedOrderDetail
              ? orderDetail()
              : Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 22),
            Text(widget._historyOrders,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 16),
            ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 10,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                    color: const Color(0xFFEFF0F0),
                    borderRadius: BorderRadius.circular(40),
                    //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0)
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(40),
                      onTap: () {
                        setState(() {
                          _isSelectedOrderDetail =
                          !_isSelectedOrderDetail;
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    const Text('Заказ #23 ',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w800,
                                            fontFamily: 'Gliroy')),
                                    const Text('(доставка)',
                                        style: TextStyle(
                                            color: redMain,
                                            fontWeight: FontWeight.w800,
                                            fontFamily: 'Gliroy'))
                                  ],
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 4, horizontal: 12),
                                  decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.circular(40),
                                    color: Colors.white,
                                    //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0)
                                  ),
                                  child: Text('Готовится',
                                      style: TextStyle(
                                          color: Color(
                                              (Random().nextDouble() *
                                                  0xFFFFFF)
                                                  .toInt())
                                              .withOpacity(1.0))),
                                )
                              ],
                            ),
                            Text('27.08.2021 (время 13:00)',
                                style: TextStyle(
                                    color: const Color(0xFFC6C7CA),
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Gliroy')),
                            const SizedBox(height: 16),
                            Text(
                                'ул. Советская 34, кв. 72, подъезд 1, домоф ул. Советская 34, кв. 72, подъезд 1, домоф',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: 'Gliroy')),
                            Text('3 товара на сумму 1664 сом',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w200,
                                    fontFamily: 'Gliroy')),
                            const SizedBox(height: 16),
                            const Text('Итого по счету: 1638 сом',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w800,
                                    fontFamily: 'Gliroy')),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
              const SizedBox(height: 16),
            ),
            const SizedBox(height: 12),
          ],
      ),
        ),
      ),
    );
  }

  Widget orderDetail() {
    final _detailOrders = 'Детали заказа';
    return Container(
      child: Column(
        children: [
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Material(
                color: Colors.transparent,
                borderOnForeground: false,
                child: InkWell(
                  enableFeedback: false,
                  borderRadius: BorderRadius.circular(40),
                  onTap: () {
                    setState(() {
                      _isSelectedOrderDetail = !_isSelectedOrderDetail;
                    });
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: SizedBox(
                        height: 16,
                        width: 16,
                        child: Image(
                            image: AssetImage('assets/images/arrow_left.png'))),
                  ),
                ),
              ),
              const Text(
                'Вернуться в корзину',
                style: TextStyle(
                    color: Color(0xFFB0B1B2),
                    fontSize: 18,
                    fontFamily: 'Gliroy'),
              )
            ],
          ),
          const SizedBox(height: 20),
          Text(_detailOrders,
              style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w800,
                  fontFamily: 'Gliroy')),
          const SizedBox(height: 16),
          Container(
            decoration: BoxDecoration(
              color: const Color(0xFFEFF0F0),
              borderRadius: BorderRadius.circular(40),
              //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0)
            ),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.circular(40),
                onTap: () {

                },
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              const Text('Заказ #23 ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w800,
                                      fontFamily: 'Gliroy')),
                              const Text('(доставка)',
                                  style: TextStyle(
                                      color: redMain,
                                      fontWeight: FontWeight.w800,
                                      fontFamily: 'Gliroy'))
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 4, horizontal: 12),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                              color: Colors.white,
                              //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0)
                            ),
                            child: Text('Готовится',
                                style: TextStyle(
                                    color: Color(
                                            (Random().nextDouble() * 0xFFFFFF)
                                                .toInt())
                                        .withOpacity(1.0))),
                          )
                        ],
                      ),
                      Text('27.08.2021 (время 13:00)',
                          style: TextStyle(
                              color: const Color(0xFFC6C7CA),
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Gliroy')),
                      const SizedBox(height: 16),
                      Text(
                          'ул. Советская 34, кв. 72, подъезд 1, домоф ул.'
                              ' Советская 34, кв. 72, подъезд 1, домоф'
                              ' Советская 34, кв. 72, подъезд 1, домоф',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Gliroy')),
                      const SizedBox(height: 12),
                      Text('Оплата: наличными',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Gliroy')),
                      const SizedBox(height: 16),
                      const Text('Итого за блюда: 1638 сом',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w800,
                              fontFamily: 'Gliroy')),
                      const Text('(бонусы для списания 146, бонусы к начислению 60)',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Gliroy')),
                      const SizedBox(height: 16),
                      const Text('Стоимость доставки: 120 сом',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w800,
                              fontFamily: 'Gliroy')),
                      const SizedBox(height: 16),
                      const Text('Итого по счету: 1638 сом',
                          style: TextStyle(
                            fontSize: 18,
                              color: Colors.black,
                              fontWeight: FontWeight.w800,
                              fontFamily: 'Gliroy')),
                      const SizedBox(height: 26),

                      Text('3 товара: Маргарита (500 гр) 2 шт., Мусульманская (1300 гр) 1 шт.',
                          style: TextStyle(
                              color: redMain,
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Gliroy')),
                      const SizedBox(height: 24),


                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
