import 'dart:math';

import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';

class OrderDetail extends StatelessWidget {
  final _detailOrders = 'Детали заказа';
  const OrderDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Material(
              color: Colors.transparent,
              borderOnForeground: false,
              child: InkWell(
                enableFeedback: false,
                borderRadius: BorderRadius.circular(40),
                onTap: () {},
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: SizedBox(
                      height: 16,
                      width: 16,
                      child: Image(
                          image:
                          AssetImage('assets/images/arrow_left.png'))),
                ),
              ),
            ),
            const Text(
              'Вернуться в корзину',
              style: TextStyle(
                  color: Color(0xFFB0B1B2),
                  fontSize: 18,
                  fontFamily: 'Gliroy'),
            )
          ],
        ),
        const SizedBox(
          height: 20
        ),
        Text(_detailOrders,
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w800,
                fontFamily: 'Gliroy')),
        const SizedBox(height: 16),
        Container(
          decoration: BoxDecoration(
            color: const Color(0xFFEFF0F0),
            borderRadius: BorderRadius.circular(40),
            //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0)
          ),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(40),
              onTap: () {
              },
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children:  [
                            const Text('Заказ #23 ',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w800,
                                    fontFamily: 'Gliroy')),
                            const Text('(доставка)',
                                style: TextStyle(
                                    color: redMain,
                                    fontWeight: FontWeight.w800,
                                    fontFamily: 'Gliroy'))
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(vertical: 4,horizontal:12 ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                            color: Colors.white,
                            //Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0)
                          ),
                          child: Text('Готовится',
                              style: TextStyle(
                                  color: Color(
                                      (Random().nextDouble() * 0xFFFFFF)
                                          .toInt())
                                      .withOpacity(1.0))),
                        )
                      ],
                    ),
                    Text('27.08.2021 (время 13:00)',
                        style: TextStyle(
                            color: const Color(0xFFC6C7CA),
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Gliroy')),
                    const SizedBox(height: 16),
                    Text('ул. Советская 34, кв. 72, подъезд 1, домоф ул. Советская 34, кв. 72, подъезд 1, домоф',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Gliroy')),
                    Text('3 товара на сумму 1664 сом',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w200,
                            fontFamily: 'Gliroy'))
                    ,
                    const SizedBox(height: 16),
                    const Text('Итого по счету: 1638 сом',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w800,
                            fontFamily: 'Gliroy')),




                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
