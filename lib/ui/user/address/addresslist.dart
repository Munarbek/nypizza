import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';


class AddressListScreen extends StatelessWidget {

  final title='Адреса доставки';

  AddressListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Material(
                  color: Colors.transparent,
                  borderOnForeground: false,
                  child: InkWell(
                    enableFeedback: false,
                    borderRadius: BorderRadius.circular(40),
                    onTap: () {},
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: SizedBox(
                          height: 16,
                          width: 16,
                          child: Image(
                              image:
                              AssetImage('assets/images/arrow_left.png'))),
                    ),
                  ),
                ),
                const Text(
                  'Вернуться в профиль',
                  style: TextStyle(
                      color: Color(0xFFB0B1B2),
                      fontSize: 18,
                      fontFamily: 'Gliroy'),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
             Text(title,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 24),

            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(13),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x0D000000),
                    offset: Offset(0.0, 4.0), //(x,y)
                    blurRadius: 5.0,
                  )
                ],
              ),
              child: Material(
                borderRadius: BorderRadius.circular(13),
                color: Colors.white,
                child: InkWell(
                  borderRadius: BorderRadius.circular(13),
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('ул. Тоголок-Молдо 5/1, кв. 56'
                          ,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Gliroy',
                            fontSize: 16,
                          ),
                        ),
                        Icon(
                          Icons.east_rounded,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 16),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(13),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x0D000000),
                    offset: Offset(0.0, 4.0), //(x,y)
                    blurRadius: 5.0,
                  )
                ],
              ),
              child: Material(
                borderRadius: BorderRadius.circular(13),
                color: Colors.white,
                child: InkWell(
                  borderRadius: BorderRadius.circular(13),
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('ул. Тоголок-Молдо 5/1, кв. 56'
                          ,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Gliroy',
                            fontSize: 16,
                          ),
                        ),
                        Icon(
                          Icons.east_rounded,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
