import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/config/constants.dart';
import 'package:nypizza/domain/model/foods.dart';
import 'package:nypizza/domain/model/foodsmodel.dart';
import 'package:nypizza/domain/model/myappmodel.dart';
import 'package:nypizza/domain/model/pizza.dart';
import 'package:nypizza/domain/model/user.dart';
import 'package:nypizza/navigation/main_navigation.dart';
import 'package:nypizza/widgets/inherited/appinherited.dart';
import 'package:nypizza/widgets/item_pizza.dart';
import 'package:nypizza/widgets/progresswidget.dart';
import 'package:provider/provider.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({Key? key}) : super(key: key);

  @override
  State<UserScreen> createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  final profile = 'Профиль';
  final name = 'Имя';
  final phone = 'Телефон';
  final status = 'Статус клиента';
  final address = 'Адреса доставки (2)';
  final contactUs = 'Связаться с нами';
  final bonusSystem = 'Условия системы бонусов';

  String? access;
  UserModelProvider? userModelProvider;

  @override
  Widget build(BuildContext context) {
    final myAppModel = AppInherited.of(context).myAppModel;
  // final user=Provider.of<User>(context).userModelProvider;
   // final box=Provider.of<User>(context).userBox;
    var box=Hive.box<UserModelProvider>(boxName);
     final user= box.get(boxUserKey);
    myAppModel.checkAuth();
    return
      // Selector<User, User?>(
      // selector: (context, user) {
      //   if (user.userModelProvider == null) {
      //     user.getUserProfile();
      //   }
      //   return user;
      // },
      // builder: (context, user, child) {
      //   return
      SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            physics: const BouncingScrollPhysics(),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(profile,
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Gliroy')),
                  Material(
                    color: Colors.transparent,
                    borderOnForeground: false,
                    child: InkWell(
                      enableFeedback: false,
                      borderRadius: BorderRadius.circular(40),
                      onTap: () {},
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: SizedBox(
                            height: 18,
                            width: 18,
                            child: Image(
                                image: AssetImage('assets/images/edit.png'))),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 16),
              Text(name,
                  style: const TextStyle(
                      color: Color(0xFFC6C7CA),
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy')),
              Text('${user?.firstName}',
                  style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w800,
                      fontFamily: 'Gliroy')),
              const SizedBox(height: 18),
              Text(phone,
                  style: const TextStyle(
                      color: Color(0xFFC6C7CA),
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy')),
              Text('${user?.phoneNumber}',
                  style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w800,
                      fontFamily: 'Gliroy')),
              const SizedBox(height: 18),
              Text(status,
                  style: const TextStyle(
                      color: Color(0xFFC6C7CA),
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy')),
              Text('${user?.status?.desc}',
                  style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w800,
                      fontFamily: 'Gliroy')),
              const SizedBox(height: 28),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(13),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x0D000000),
                      offset: Offset(0.0, 4.0), //(x,y)
                      blurRadius: 5.0,
                    )
                  ],
                ),
                child: Material(
                  borderRadius: BorderRadius.circular(13),
                  color: Colors.white,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(13),
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            address,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Gliroy',
                              fontSize: 16,
                            ),
                          ),
                          const Icon(
                            Icons.east_rounded,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 16),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(13),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x0D000000),
                      offset: Offset(0.0, 4.0), //(x,y)
                      blurRadius: 5.0,
                    )
                  ],
                ),
                child: Material(
                  borderRadius: BorderRadius.circular(13),
                  color: Colors.white,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(13),
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            contactUs,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Gliroy',
                              fontSize: 16,
                            ),
                          ),
                          const Icon(
                            Icons.east_rounded,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 16),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(13),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x0D000000),
                      offset: Offset(0.0, 4.0), //(x,y)
                      blurRadius: 5.0,
                    )
                  ],
                ),
                child: Material(
                  borderRadius: BorderRadius.circular(13),
                  color: Colors.white,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(13),
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            bonusSystem,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Gliroy',
                              fontSize: 16,
                            ),
                          ),
                          const Icon(
                            Icons.east_rounded,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 26),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Material(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(13),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(13),
                      onTap: () {
                        if (myAppModel.isAuth) {
                          myAppModel.deletePhoneNumber();
                          box.delete(boxUserKey);
                        }

                        Navigator.of(context)
                            .pushReplacementNamed(MainNavigationRouteName.auth);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: const [
                            Text('Выход',
                                style: TextStyle(
                                    color: redMain,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Gliroy')),
                            SizedBox(width: 6),
                            Icon(Icons.logout_rounded, color: redMain),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 46),
            ]));

  }
}

/*Column(
          children: [
            ElevatedButton(
                onPressed: () {
                  foodsModel?.repoGetFoods();
                },
                child: const Text('Get Foods')),

            if (foods != null) Expanded(
              child: ListView.builder(
                itemCount: foods.results?.length,
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.only(bottom: 12),
                  child: ItemPizzaHome(
                      pizza: Pizza(
                        '${foods.results![index].ID}',
                     '${foods.results![index].category!.name}',
                        '${foods.results![index].name}',
                    '${foods.results![index].gram} гр',
                        foods.results![index].gram!.toInt(),
                        false,
                    'assets/images/newyorkbig.png',
                    'соус, сыр тильзитский, говядина, курица копченная',
                  )),
                ),
              ),
            ) else const Expanded(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ],
        )
*
* */
