import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';


class ContactUsScreen extends StatelessWidget {

  final title='Свяжитесь с нами';
  final searchInMap='Найти нас на карте';
  final contact='Контакты (прямой номер 1771)';
  final whatsApp='Написать в WhatsApp';
  final callMobile='Позвонить на мобильный';
  final callCity='Позвонить на городской';
  final instagram='nypizza.kg';


  ContactUsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Material(
                  color: Colors.transparent,
                  borderOnForeground: false,
                  child: InkWell(
                    enableFeedback: false,
                    borderRadius: BorderRadius.circular(40),
                    onTap: () {},
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: SizedBox(
                          height: 16,
                          width: 16,
                          child: Image(
                              image:
                              AssetImage('assets/images/arrow_left.png'))),
                    ),
                  ),
                ),
                const Text(
                  'Вернуться в профиль',
                  style: TextStyle(
                      color: Color(0xFFB0B1B2),
                      fontSize: 18,
                      fontFamily: 'Gliroy'),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Text(title,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 24),
            Text(searchInMap,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),

            const SizedBox(height: 24),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(13),
              color: const Color(0xFFB0B1B2),
            ),

            width: double.infinity,
            height: 113,
            child: Center(
              child: Text(
                'Map'
              ),
            ),
          ),
            const SizedBox(height: 36),
            Text(contact,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
            const SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(43),
                border: Border.all(color: redMain, width: 1),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(43),
                color: Colors.white,
                child: InkWell(
                  borderRadius: BorderRadius.circular(43),
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 24,
                          width: 24,
                          child: Image(image: AssetImage('assets/images/whatsapp.png'),),
                        ),
                        const SizedBox(width: 4,),
                        Text(whatsApp,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Gliroy',
                            fontSize: 16,
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),

            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(43),
                border: Border.all(color: redMain, width: 1),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(43),
                color: Colors.white,
                child: InkWell(
                  borderRadius: BorderRadius.circular(43),
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                    const   Icon(
                      Icons.phone_iphone_rounded,color: redMain,
                    ),
                        const SizedBox(width: 4,),
                        Text(callMobile,
                          style: const TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Gliroy',
                            fontSize: 16,
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(43),
                border: Border.all(color: redMain, width: 1),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(43),
                color: Colors.white,
                child: InkWell(
                  borderRadius: BorderRadius.circular(43),
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                     const  Icon(
                      Icons.call_rounded,color:redMain
                    ),
                        const SizedBox(width: 4,),
                        Text(callCity,
                          style: const TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Gliroy',
                            fontSize: 16,
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 40),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Material(
                  color:  Colors.transparent,
                  borderRadius: BorderRadius.circular(13),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(13),
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children:  [
                          const SizedBox(
                            height: 24,
                            width: 24,
                            child: Image(image: AssetImage('assets/images/Instagram.png'),),
                          ),

                           const SizedBox(width:6),
                          Text(instagram,
                              style:  const TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontFamily: 'Gliroy')),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 56),
          ],
        ),
      ),
    );
  }
}
