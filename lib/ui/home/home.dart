import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/domain/data/securitystorage/security_storage.dart';
import 'package:nypizza/domain/model/category.dart';
import 'package:nypizza/domain/model/food.dart';
import 'package:nypizza/domain/model/foods.dart';
import 'package:nypizza/domain/model/foodsmodel.dart';
import 'package:nypizza/domain/model/homestate.dart';
import 'package:nypizza/domain/model/pizza.dart';
import 'package:nypizza/ui/basked/basked.dart';
import 'package:nypizza/ui/example/ex.dart';
import 'package:nypizza/widgets/avatar.dart';
import 'package:nypizza/widgets/food_categories_item.dart';
import 'package:nypizza/widgets/item_pizza.dart';
import 'package:nypizza/widgets/progresswidget.dart';
import 'package:nypizza/widgets/stocklist.dart';
import 'package:provider/provider.dart';

final dataKey = GlobalKey();

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  var _tabController;
  int tabIndex = 0;
  var menuIndex = 0;
  bool isLoading = true;
  final _securityStorage = SecurityStorage();

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 8);
    super.initState();
  }

  late HomeState _homeState;
  late FoodsModel _foodsModel;

  final list = [];

  @override
  Widget build(BuildContext context) {
    print('Build  Home Screen ');
    return ChangeNotifierProvider<HomeState>(
      create: (context) => HomeState(),
      child: Consumer<HomeState>(
        builder: (_, homeState, __) {
          _homeState = homeState;
          tabIndex = _homeState.categorySelectedIndex;
          return Container(
            color: background,
            child: Selector<FoodsModel, Foods?>(
              selector: (_, foodsmodel) {
                _foodsModel = foodsmodel;
                if (foodsmodel.getFoods == null &&
                    foodsmodel.errorMessage == null) {
                  foodsmodel.fetchHomeContent();
                }
                return foodsmodel.getFoods;
              },
              builder: (context, foods, child) {
                return CustomScrollView(
                    physics: const BouncingScrollPhysics(),
                    slivers: foods != null || _foodsModel.errorMessage != null
                        ? foods != null
                            ? [
                                const SliverToBoxAdapter(
                                  child: TextField(
                                    decoration: InputDecoration(
                                        icon: Padding(
                                          padding: EdgeInsets.only(left: 20.0),
                                          child: Icon(Icons.search_rounded),
                                        ),
                                        border: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        hintText: 'Что ты желаешь сегодня?'),
                                  ),
                                ),
                                const SliverToBoxAdapter(
                                    child: Padding(
                                  padding:
                                      EdgeInsets.only(left: 20, bottom: 12),
                                  child: Text('Акции',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w800,
                                          fontFamily: 'Gliroy')),
                                )),
                                SliverToBoxAdapter(
                                  child: SizedBox(
                                    height: 140,
                                    child: ListView.builder(
                                        physics: const BouncingScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0),
                                            child: InkWell(
                                              onTap: _showBottomDialog,
                                              child: Container(
                                                width: 210,
                                                height: 135,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(13),
                                                ),
                                                child: Column(
                                                  children: const [
                                                    SizedBox(
                                                        height: 100,
                                                        width: 210,
                                                        child: Image(
                                                          image: AssetImage(
                                                              'assets/images/rectangle.png'),
                                                          fit: BoxFit.cover,
                                                        )),
                                                    Padding(
                                                      padding: EdgeInsets.only(
                                                          right: 8.0,
                                                          left: 8.0),
                                                      child: Text(
                                                          'Закажи 3 пиццы и получи одну бесплатно!',
                                                          maxLines: 2,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            fontFamily:
                                                                'Gliroy',
                                                            fontSize: 14,
                                                          )),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                        itemCount: 2),
                                  ),
                                ),
                                CurrySliverHeader2(),
                                menuScreen(
                                    dataKey,
                                    _foodsModel.getFoods?.results,
                                    _foodsModel.categories),
                              ]
                            : [
                                SliverFillRemaining(
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                      Text('${_foodsModel.errorMessage}'),
                                      IconButton(
                                        icon: Image.asset(
                                            'assets/images/reset.png'),
                                        iconSize: 50,
                                        onPressed: () {
                                          _foodsModel.changeLoading();
                                        },
                                      )
                                    ]))
                              ]
                        : [
                            const SliverFillRemaining(
                                child: Center(child: SizedBox(
                                    height: 160,
                                    width: 160,
                                    child: RiveAnimationPage())))
                          ]);
              },
            ),
          );
        },
      ),
    );
  }

  SliverToBoxAdapter menuScreen(
      GlobalKey dataKey, List<Food>? results, List<Category> categories) {
    final scrollController = ScrollController();
    _tabController = TabController(vsync: this, length: results?.length ?? 0);
    return SliverToBoxAdapter(
      key: dataKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text('Категории блюд',
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Gliroy')),
          ),
          TabBar(
            onTap: (value) {},
            controller: _tabController,
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
            indicatorPadding: const EdgeInsets.all(0),
            isScrollable: true,
            physics: const BouncingScrollPhysics(),
            unselectedLabelColor: const Color(0xFF828282),
            labelColor: blackMain,
            labelStyle: const TextStyle(fontWeight: FontWeight.w700),
            labelPadding: const EdgeInsets.symmetric(horizontal: 8.0),
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x26000000),
                    offset: Offset(0.0, 4.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: yellowMain)),
            tabs: List.generate(results?.length ?? 0, (index) {
              return FoodCategoriesItem(Category(
                  id: categories[index].id,
                  name: categories[index].name,
                  cover_file: categories[index].cover_file));
            }),
          ),
          const SizedBox(height: 20),
          ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: results?.length ?? 0,
            itemBuilder: (context, index) {
              return ItemPizzaHome(pizza: results?.elementAt(index));
            },
            separatorBuilder: (context, index) => const SizedBox(height: 14),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  /*SliverToBoxAdapter popularScreen(HomeState _homeState, GlobalKey dataKey) {
    final scrollController = ScrollController();
    return SliverToBoxAdapter(
      key: dataKey,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const Padding(
          padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
          child: Text('Популярные блюда',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w800,
                  fontFamily: 'Gliroy')),
        ),
        ListView.builder(
          itemCount: 15,
          shrinkWrap: true,
          controller: scrollController,
          itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.only(bottom: 12),
            child: ItemPizzaHome(
                pizza: results?.elementAt(index)),
          ),
        ),
      ]),
    );
  }

  SliverToBoxAdapter favoriteScreen(HomeState _homeState, GlobalKey dataKey) {
    final scrollController = ScrollController();
    return SliverToBoxAdapter(
      key: dataKey,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const Padding(
          padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
          child: Text('Твои избранные блюда',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w800,
                  fontFamily: 'Gliroy')),
        ),
        ListView.builder(
          itemCount: 15,
          shrinkWrap: true,
          controller: scrollController,
          itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.only(bottom: 12),
            child: ItemPizzaHome(
                pizza: Pizza(
              '${Random().nextInt(1000)}',
              'Pizza',
              'Нью-Йорк',
              '650 гр',
              658,
              false,
              'assets/images/newyorkbig.png',
              'соус, сыр тильзитский, говядина, курица копченная',
            )),
          ),
        ),
      ]),
    );
  }*/

  void _showBottomDialog() {
    showModalBottomSheet<void>(
      elevation: 15,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return Container(
            height: 516,
            decoration: const BoxDecoration(
                color: background,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(32),
                    topRight: Radius.circular(32))),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 10, left: 20, right: 20, bottom: 56),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('Акции',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w800,
                              fontFamily: 'Gliroy')),
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(
                          Icons.close_rounded,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            height: 152,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(18),
                            ),
                            child: Column(
                              children: const [
                                ClipRRect(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(13),
                                      topRight: Radius.circular(13)),
                                  child: Image(
                                    height: 110,
                                    width: double.infinity,
                                    image: AssetImage(
                                        'assets/images/rectangle.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Padding(
                                  padding:
                                      EdgeInsets.only(right: 8.0, left: 8.0),
                                  child: SizedBox(
                                    height: 40,
                                    child: Center(
                                      child: Text(
                                          'Закажи 3 пиццы и получи одну бесплатно!',
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'Gliroy',
                                            fontSize: 14,
                                          )),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          const Text(
                              'Четвертая пицца за наш счет! Закажите четыре любые пиццы, и одну из них мы вам подарим. Положите в корзину 4 пиццы, введите и примените промокод 3PLUS1. При заказе по тел. 500-040 назовите промокод оператору.',
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Gliroy',
                                fontSize: 14,
                              )),
                          const SizedBox(
                            height: 24,
                          ),
                          const Text(
                              '*В подарок – пицца с наименьшей стоимостью. В акции не участвуют пиццы на одного. Акция действует только на доставке, не суммируется с другими акциями.',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Gliroy',
                                fontSize: 14,
                              )),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ));
      },
    );
  }
}

//  SliverList(
//         delegate: SliverChildBuilderDelegate((context, index) {
//           return Padding(
//             padding: const EdgeInsets.only(bottom: 12),
//             child: ItemPizzaHome(
//                 pizza: Pizza(
//               '${Random().nextInt(1000)}',
//               'Pizza',
//               'Нью-Йорк',
//               '650 гр',
//               658,
//               false,
//               'assets/images/newyorkbig.png',
//               'соус, сыр тильзитский, говядина, курица копченная',
//             )),
//           );
//         }, childCount: 20),
//       )

class CurrySliverHeader2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('Build menu Home');
    return SliverPersistentHeader(
      pinned: true,
      floating: false,
      delegate: Delegate2(),
    );
  }
}

class Delegate2 extends SliverPersistentHeaderDelegate {
  late HomeState _homeState;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Selector<HomeState, int>(
      selector: (_, provider) {
        _homeState = provider;
        return provider.menuSelectedIndex;
      },
      builder: (context, menuSelectedIndex, child) {
        return Container(
          color: background,
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Material(
                borderRadius: BorderRadius.circular(10),
                color: backgroundItemMenu,
                child: InkWell(
                  splashColor: background,
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 15),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white, width: 1),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Text(
                      'Меню',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: menuSelectedIndex == 0
                            ? redMain
                            : const Color(0xFF828282),
                        fontFamily: 'Gliroy',
                        fontSize: 15,
                      ),
                    ),
                  ),
                  onTap: () {
                    _homeState.setMenuSelectedIndex(0);
                    Scrollable.ensureVisible(dataKey.currentContext!,
                        duration: const Duration(milliseconds: 800),
                        curve: Curves.fastOutSlowIn);
                  },
                ),
              ),
              Material(
                  borderRadius: BorderRadius.circular(10),
                  color: backgroundItemMenu,
                  child: InkWell(
                    onTap: () {
                      _homeState.setMenuSelectedIndex(1);
                      Scrollable.ensureVisible(dataKey.currentContext!,
                          duration: const Duration(milliseconds: 800));
                    },
                    splashColor: background,
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 12, horizontal: 15),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 1),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Text(
                        'Популярное',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: menuSelectedIndex == 1
                              ? redMain
                              : const Color(0xFF828282),
                          fontFamily: 'Gliroy',
                          fontSize: 15,
                        ),
                      ),
                    ),
                  )),
              Material(
                borderRadius: BorderRadius.circular(10),
                color: backgroundItemMenu,
                child: InkWell(
                  onTap: () {
                    _homeState.setMenuSelectedIndex(2);
                    Scrollable.ensureVisible(dataKey.currentContext!,
                        duration: const Duration(milliseconds: 800),
                        curve: Curves.fastOutSlowIn);
                  },
                  splashColor: background,
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 15),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white, width: 1),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Text(
                      'Избранное',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: menuSelectedIndex == 2
                            ? redMain
                            : const Color(0xFF828282),
                        fontFamily: 'Gliroy',
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  double get maxExtent => 62;

  @override
  double get minExtent => 62;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
