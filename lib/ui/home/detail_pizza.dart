import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nypizza/config/colors.dart';

class DetalPizzaScreen extends StatelessWidget {
   DetalPizzaScreen({Key? key}) : super(key: key);
  var isPopular=true;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Material(
                color: Colors.transparent,
                borderOnForeground: false,
                child: InkWell(
                  enableFeedback: false,
                  borderRadius: BorderRadius.circular(40),
                  onTap: () {},
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: SizedBox(
                        height: 16,
                        width: 16,
                        child: Image(
                            image: AssetImage('assets/images/arrow_left.png'))),
                  ),
                ),
              ),
              const Text(
                'Вернуться на главную',
                style: TextStyle(
                    color: Color(0xFFB0B1B2),
                    fontSize: 18,
                    fontFamily: 'Gliroy'),
              )
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Stack(children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Image(
                image: AssetImage('assets/images/imagePizzaMenu.png'),
                height: 300,
              ),
            ),

            if(isPopular)
              Positioned(
                bottom: 28,
                right: 0,
                  child: Container(
                padding: const EdgeInsets.all(8),
                decoration: const BoxDecoration(
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x26000000),
                        offset: Offset(0.0, 4.0), //(x,y)
                        blurRadius: 5.0,
                      ),
                    ],

                  borderRadius: BorderRadius.only(topLeft: Radius.circular(13),bottomRight: Radius.circular(13)),
                  color:primaryOrange
                ),
                child: Text('Популярное',style: TextStyle(
                  color:  Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold
                ),),
              ))



          ]),
          const SizedBox(
            height: 24,
          ),
          Text('Американо (700 гр)',
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 22,
                  fontWeight: FontWeight.w800,
                  fontFamily: 'Gliroy')),
          const SizedBox(
            height: 12,
          ),
          Text(
              'два вида соуса, сыр тильзитский, говядина, курица копченая, перец, помидоры, оливки, орегано (много мяса).',
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Gliroy')),
          const SizedBox(
            height: 24,
          ),
          const Text(
            'Размер/Цена',
            style: TextStyle(
                color: Color(0xFFB0B1B2), fontSize: 18, fontFamily: 'Gliroy'),
          ),
          const SizedBox(
            height: 8,
          ),
          Container(
            decoration: BoxDecoration(
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x26000000),
                    offset: Offset(0.0, 4.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: yellowMain)),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: [
                  Text('700 гр',
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Gliroy')),
                  Text('700 сом',
                      style: const TextStyle(
                          color: yellowMain,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Gliroy'))
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 36,
          ),
          SizedBox(
            width: double.infinity,
            height: 45,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: redMain,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30), // <-- Radius
                  ),
                ),
                onPressed: () {},
                child: Text('В корзину за 758 сом',
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Gliroy',
                      fontSize: 15,
                    ))),
          ),
          const SizedBox(
            height: 36,
          ),
        ],
      ),
    );
  }
}
