import 'package:flutter/material.dart';

class QrScreen extends StatelessWidget {
  const QrScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('123 343',style: const TextStyle(
            fontWeight: FontWeight.w700,
            fontFamily: 'Gliroy',
            fontSize: 28,
          ),),
          Container(
              width: 243,
              height: 243,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32.0),
                color:const Color(0xFFEFEFEF),
              ),
              child: Padding(
                padding: const EdgeInsets.all(28.0),
                child: Image.network(
                    'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=Example',fit: BoxFit.cover, ),
              )),
          Text('Предъявите официанту Ваш QR или сообщите оператору код',style: const TextStyle(
            fontWeight: FontWeight.w600,
            fontFamily: 'Gliroy',
            fontSize: 18,

          ),textAlign: TextAlign.center,),
        ],
      ),
    );
  }
}
