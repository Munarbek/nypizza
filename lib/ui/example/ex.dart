// import 'package:flutter/material.dart';
//
//
//
// class PageState extends StatefulWidget {
//   const PageState({Key? key}) : super(key: key);
//
//   @override
//   _PageStateState createState() => _PageStateState();
// }
//
// class _PageStateState extends State<PageState> {
//   late GlobalKey<_ItemFaderState> key;
//
//   @override
//   void initState() {
//     super.initState();
//     key = GlobalKey<_ItemFaderState>();
//     init();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: ItemFader(
//           key: key,
//           child: InkWell(
//               onTap: onTab, child: Center(child: const Text('Widget one'))),
//         ),
//       ),
//     );
//   }
//
//   void onTab() {
//     hide();
//   }
//
//   void init() async {
//     await Future.delayed(const Duration(milliseconds: 40));
//     key.currentState?.show();
//   }
//
//   void hide() async {
//     await Future.delayed(const Duration(milliseconds: 40));
//     key.currentState?.hide();
//   }
// }
//
// class FlightsStepperState extends StatefulWidget {
//   const FlightsStepperState({Key? key}) : super(key: key);
//
//   @override
//   _FlightsStepperStateState createState() => _FlightsStepperStateState();
// }
//
// class _FlightsStepperStateState extends State<FlightsStepperState> {
//   late GlobalKey<_ItemFaderState> key;
//
//   @override
//   void initState() {
//     super.initState();
//     key = GlobalKey<_ItemFaderState>();
//     key.currentState?.show();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: ItemFader(
//           child:
//               Center(child: InkWell(onTap: () {}, child: const Text('Text 2'))),
//         ),
//       ),
//     );
//   }
// }
