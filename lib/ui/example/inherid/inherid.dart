import 'package:flutter/material.dart';

class ExamInherid extends StatelessWidget {
  const ExamInherid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DataOwnerStateFull();
  }
}


class  DataOwnerStateFull extends StatefulWidget {
  const DataOwnerStateFull({Key? key}) : super(key: key);

  @override
  State<DataOwnerStateFull> createState() => _DataOwnerStateFullState();
}

class _DataOwnerStateFullState extends State<DataOwnerStateFull> {

 var _value=10;

 void _increment(){
   setState((){
     _value++;
   });
 }

  @override
  Widget build(BuildContext context) {
   print('Build DataOwnerStateFull');
    return Column(
      children: [
        ElevatedButton(onPressed: _increment, child: const Text('Add')),
        DataProviderInherite(
            value: _value,
            child: const  DataConsumerStateLess()),
      ],
    );
  }
}

class DataConsumerStateLess extends StatelessWidget {
  const DataConsumerStateLess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('Build DataConsumerStateLess');
    final value=context.dependOnInheritedWidgetOfExactType<DataProviderInherite>()?.value ?? 0;
    return Column(
      children: [
        Text('$value'),
          const DataConsumerStateFull()
      ],
    );
  }
}

class DataConsumerStateFull extends StatefulWidget {
  const DataConsumerStateFull({Key? key}) : super(key: key);

  @override
  _DataConsumerStateFullState createState() => _DataConsumerStateFullState();
}

class _DataConsumerStateFullState extends State<DataConsumerStateFull> {
  @override
  Widget build(BuildContext context) {
    print('DataConsumerStateFull');
    final widget=context.getElementForInheritedWidgetOfExactType<DataProviderInherite>()?.widget as DataProviderInherite ;
    final value=widget.value;
    return Container(child: Text('$value'),);
  }
}




class DataProviderInherite extends InheritedWidget{
  final int value;
  const DataProviderInherite({Key? key,required Widget child,required this.value}) : super(key: key,child: child);

  @override
  bool updateShouldNotify(covariant DataProviderInherite oldWidget) {
    return value!=oldWidget.value;
  }
}