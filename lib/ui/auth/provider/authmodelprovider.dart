import 'package:flutter/material.dart';
import 'package:nypizza/domain/model/numberverify.dart';
import 'package:nypizza/domain/respository/repository.dart';

class AuthModelProvider extends ChangeNotifier {
  AuthModelProvider(this._repository);

  String? _phoneNumber;
  bool _isLoadingAuth = false;
  bool _isLoadingAuthCode = false;
  bool _successPhoneNumber = false;
  NumberVerify? get numberVerify => _numberVerify;
  bool get isLoadingAuthCode => _isLoadingAuthCode;
  NumberVerify? _numberVerify;

  bool get successPhoneNumber => _successPhoneNumber;
  String? _errorMessage;
  String? _errorMessageCode;
  final Repository _repository;

  bool get isLoadingAuth => _isLoadingAuth;

  String? get phoneNumber => _phoneNumber;

  set setIsLoadingAuth(bool value) {
    _isLoadingAuth = value;
  }

  String? get getErrorMessage => _errorMessage;

  String? get getErrorMessageCode => _errorMessageCode;

  void sendSmsCode(String phoneNumber) async {
    changeIsLoadingAuth();
    _repository.sendSmsCode(phoneNumber).then((value) {
      _errorMessage = null;
      _successPhoneNumber = true;
      _phoneNumber = phoneNumber;
      notifyListeners();
    }).catchError((value) {
      _successPhoneNumber = true;
      _errorMessage = 'Что-то пошло не так.Попробуйте ещё раз';
      notifyListeners();
    });
  }

  void verify(String phoneNumber, String code) async {
    changeIsLoadingAuthCode();
    _repository.verify(phoneNumber, code).then((value) {
      _numberVerify = value;
      _isLoadingAuthCode = false;
      _errorMessageCode = null;
      print('success');
      notifyListeners();
    }).catchError((value) {
      _errorMessageCode = 'Что-то пошло не так.Попробуйте ещё раз';
      print('error');
      _isLoadingAuthCode = false;
      _numberVerify = null;
      notifyListeners();
    });
  }

  void sendSmsCodeRepeat(String phoneNumber) async {
    _repository
        .sendSmsCode(phoneNumber)
        .then((value) {

    })
        .catchError((value) {

    });
  }

  void setErrorMessage(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  void setErrorMessageCode(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  set setIsLoadingAuthCode(bool value) {
    _isLoadingAuthCode = value;
  }

  void changeIsLoadingAuth() {
    _successPhoneNumber = false;
    _isLoadingAuth = !isLoadingAuth;
    _errorMessage = null;
    _phoneNumber = null;
    notifyListeners();
  }

  void changeIsLoadingAuthCode() {
    _isLoadingAuth = !isLoadingAuth;
    _errorMessageCode = null;
    notifyListeners();
  }
}
