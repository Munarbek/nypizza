import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/domain/model/numberverify.dart';
import 'package:nypizza/navigation/main_navigation.dart';
import 'package:nypizza/ui/auth/provider/authmodelprovider.dart';
import 'package:nypizza/widgets/inherited/appinherited.dart';
import 'package:nypizza/widgets/progresswidget.dart';
import 'package:provider/provider.dart';

class AuthSmsScreen extends StatelessWidget {
  final String phoneNumber;


  const AuthSmsScreen({Key? key, required this.phoneNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('codeParent');
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: background,
      ),
    );
    final  _repository=AppInherited.of(context);
    final  _securityStorage=AppInherited.of(context);
    return ChangeNotifierProvider<AuthModelProvider>(
        create: (context) => AuthModelProvider(_repository.repository),
        builder: (context, child) => Selector<AuthModelProvider, NumberVerify?>(
              builder: (context, numberVerify, child) {
                if (numberVerify != null) {
                  _securityStorage.securityStorage.setPhoneNumber(phoneNumber);
                  _securityStorage.securityStorage.setAccessToken(numberVerify.access);
                  _securityStorage.securityStorage.setRefreshToken(numberVerify.refresh);
                  WidgetsBinding.instance?.addPostFrameCallback((_) {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        MainNavigationRouteName.app, (r) => false,
                        arguments: phoneNumber);
                  });
                }
                return Scaffold(
                  body: SafeArea(
                    child: Container(
                      height: double.infinity,
                      color: background,
                      child: AuthorizationSmsScreen(phoneNumber: phoneNumber),
                    ),
                  ),
                );
              },
              selector: (_, authModelProvider) => authModelProvider.numberVerify,
            ));
  }

// authModel.isLoadingAuth ? const Center(child: RiveAnimationPage()):phoneNumberWidget(authModel)

}

class AuthorizationSmsScreen extends StatefulWidget {
  final title = 'Рады видеть тебя!';
  final header = 'Вход в аккаунт';
  final enterNumber = 'Введите ваш номер телефона';
  final next = 'Продолжить';
  final skip = 'Пропустить';
  final sms = 'Введите код из СМС';
  final time = 'У вас осталось';
  final send = 'Отправлен на номер';
  final newCodeGet = 'Получить новый код';
  final repeat = '';
  final String phoneNumber;

  const AuthorizationSmsScreen({Key? key, required this.phoneNumber})
      : super(key: key);

  @override
  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationSmsScreen> {
  var isEnter = false;
  String phoneNumber = '';
  final controller = TextEditingController();
  var isTimeOut = false;
  var changeWidget = false;
  bool isLoading = false;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    phoneNumber = widget.phoneNumber;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('codeChild');
    final authModel = Provider.of<AuthModelProvider>(context);
    return authModel.isLoadingAuthCode
        ? const Center(child: RiveAnimationPage())
        : SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamedAndRemoveUntil(
                          context, MainNavigationRouteName.app,
                          (r)=>false,
                          arguments:null);
                    },
                    child: Text(widget.skip,
                        style: const TextStyle(
                            color: Color(0xFFB0B1B2),
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Gliroy')),
                  ),
                ),
                const SizedBox(height: 22),
                const SizedBox(height: 22),
                Text(widget.sms,
                    style: const TextStyle(
                        color: Colors.black,
                        fontSize: 23,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Gliroy')),
                const SizedBox(height: 8),
                Text('${widget.send} $phoneNumber',
                    style: const TextStyle(
                        color: Color(0xFFB0B1B2),
                        fontWeight: FontWeight.w400,
                        fontFamily: 'Gliroy')),
                const SizedBox(height: 18),
                OtpTextField(
                  numberOfFields: 6,
                  hasCustomInputDecoration: true,
                  decoration: const InputDecoration(
                    counterText: '',
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFFB0B1B2), width: 2.0)),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: redMain, width: 2.0)),
                  ),
                  //runs when a code is typed in
                  onCodeChanged: (String code) {
                    //handle validation or checks here
                  },
                  //runs when every textfield is filled
                  onSubmit: (String verificationCode) {
                    if(verificationCode.replaceAll(' ', '').length==6) {
                      authModel.verify(phoneNumber, verificationCode);
                    }
                  }, // end onSubmit
                ),
                const SizedBox(height: 16),
                isTimeOut
                    ? Center(
                        child: Text(widget.repeat,
                            style: const TextStyle(
                                color: redMain,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'Gliroy')),
                      )
                    : const Tic(),
                const SizedBox(height: 12),
                SizedBox(
                  height: 16,
                  child: Text(authModel.getErrorMessageCode ?? '',
                      style: const TextStyle(
                          color: redMain,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Gliroy')),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  width: double.infinity,
                  height: 45,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: isTimeOut ? redMain : disable,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30), // <-- Radius
                        ),
                      ),
                      onPressed: () {


                        setState(() {
                          if (isTimeOut) {
                            authModel.sendSmsCodeRepeat(phoneNumber);
                            setIsTimeOut();
                          }
                        });
                      },
                      child: Text(widget.newCodeGet,
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Gliroy',
                            fontSize: 15,
                          ))),
                ),
              ],
            ),
          );
  }

  void setIsTimeOut() {
    setState(() {
      isTimeOut = !isTimeOut;
    });
  }
}

class Tic extends StatefulWidget {
  final time = 'У вас осталось';

  const Tic({
    Key? key,
  }) : super(key: key);

  @override
  _TicState createState() => _TicState();
}

class _TicState extends State<Tic> {
  int minute = 4;
  int second = 0;
  int count = 0;
  late Timer timer;
  _AuthorizationScreenState? parentState;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(const Duration(milliseconds: 1000), (timer) {
      tick();
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    parentState = context.findAncestorStateOfType<_AuthorizationScreenState>();
    return Align(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(widget.time,
              style: const TextStyle(
                  color: Color(0xFFB0B1B2),
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Gliroy')),
          Text(' 0$minute : ${second > 9 ? second : '0$second'}',
              style: const TextStyle(
                  color: Color(0xFFB0B1B2),
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Gliroy')),
        ],
      ),
    );
  }

  void tick() {
    setState(() {
      count++;
      second = 59 - count % 60;
      if (second == 0 && minute > 0) {
        minute--;
      } else if (second == 0 && minute == 0) {
        parentState?.setIsTimeOut();
        timer.cancel();
      }
    });
  }
}
