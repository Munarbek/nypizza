import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:nypizza/apiservice/apiclient.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/domain/data/securitystorage/security_storage.dart';
import 'package:nypizza/domain/model/foodsmodel.dart';
import 'package:nypizza/domain/respository/repository.dart';
import 'package:nypizza/navigation/main_navigation.dart';
import 'package:nypizza/ui/auth/provider/authmodelprovider.dart';
import 'package:nypizza/widgets/inherited/appinherited.dart';
import 'package:nypizza/widgets/progresswidget.dart';
import 'package:provider/provider.dart';
import '../../app.dart';

class AuthScreen extends StatelessWidget {


 const  AuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: background,
      ),
    );

    final  _repository=AppInherited.of(context);
    return ChangeNotifierProvider<AuthModelProvider>(
        create: (context) => AuthModelProvider(_repository.repository),
        builder: (context, child) => Selector<AuthModelProvider, String?>(
              builder: (context, value, child) {
                if (value!=null) {
                  WidgetsBinding.instance?.addPostFrameCallback((_) {
                    Navigator.of(context).pushNamed(
                        MainNavigationRouteName.authSms,
                        arguments: value);
                  });
                }
                return Scaffold(
                  body: SafeArea(
                    child: Container(
                      height: double.infinity,
                      color: background,
                      child: const AuthorizationScreen(),
                    ),
                  ),
                );
              },
              selector: (_, authModelProvider) => authModelProvider.phoneNumber,
            ));
  }

// authModel.isLoadingAuth ? const Center(child: RiveAnimationPage()):phoneNumberWidget(authModel)

}

class AuthorizationScreen extends StatefulWidget {
  final title = 'Рады видеть тебя!';
  final header = 'Вход в аккаунт';
  final enterNumber = 'Введите ваш номер телефона';
  final next = 'Продолжить';
  final skip = 'Пропустить';
  final sms = 'Введите код из СМС';
  final time = 'У вас осталось';
  final send = 'Отправлен на номер';
  final newCodeGet = 'Получить новый код';
  final repeat = 'Попробуйте еще раз';

  const AuthorizationScreen({Key? key}) : super(key: key);

  @override
  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  var isEnter = false;
  String phoneNumber = '';
  final controller = TextEditingController();
  var isTimeOut = false;
  final _securityStorage = SecurityStorage();
  var changeWidget = false;
  FoodsModel? _foodsmodel;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('Build parent auth');
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: background,
      ),
    );
    return Consumer<AuthModelProvider>(
      builder: (context, authModel, child) {
        return authModel.isLoadingAuth
            ? const Center(child: RiveAnimationPage())
            : phoneNumberWidget(authModel);
      },
    );
  }

  Widget phoneNumberWidget(AuthModelProvider authModel) {
    print('Build parent child');
    print(authModel.getErrorMessage);
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, MainNavigationRouteName.app,
                    (r)=>false,
                    arguments: null);
              },
              child: Text(widget.skip,
                  style: const TextStyle(
                      color: Color(0xFFB0B1B2),
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Gliroy')),
            ),
          ),
          const SizedBox(height: 22),
          Text(widget.title,
              style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Gliroy')),
          const SizedBox(height: 22),
          Row(
            children: [
              Text(widget.header,
                  style: const TextStyle(
                      fontSize: 25,
                      color: Colors.black,
                      fontWeight: FontWeight.w800,
                      fontFamily: 'Gliroy')),
              const SizedBox(width: 8),
              const Image(
                image: AssetImage('assets/images/image_phone.png'),
                height: 24,
                width: 24,
              )
            ],
          ),
          const SizedBox(height: 42),
          Text(widget.enterNumber,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Gliroy')),
          const SizedBox(height: 42),
          Row(
            children: [
              const Text('+ 996',
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Gliroy')),
              const SizedBox(width: 8),
              Expanded(
                child: TextFormField(
                  controller: controller,
                  onChanged: (value) {
                    phoneNumber = value;
                    if (value.length > 11) {
                      setState(() {
                        if (!isEnter) isEnter = !isEnter;

                      });
                    } else {
                      setState(() {
                        changeWidget = false;
                        isEnter = false;
                      });
                    }
                  },
                  style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Gliroy'),
                  keyboardType: TextInputType.phone,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(12),
                    MaskTextInputFormatter(
                        mask: '000 00 00 00',
                        filter: {"0": RegExp(r'[0-9]')},
                        initialText: controller.text),
                  ],
                  decoration: const InputDecoration(
                      counterText: '',
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      hintStyle: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Gliroy'),
                      hintText: '000 00 00 00'),
                ),
              )
            ],
          ),
          const SizedBox(height: 12),
          SizedBox(
            height: 20,
            child: Text(authModel.getErrorMessage ?? '',
                style: const TextStyle(
                    color: redMain,
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Gliroy')),
          ),
          const SizedBox(height: 10),
          SizedBox(
            width: double.infinity,
            height: 45,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: isEnter ? redMain : disable,
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30), // <-- Radius
                  ),
                ),
                onPressed: () {
                  if (phoneNumber.length > 11) {
                    // setState(() {
                    //   changeWidget=true;
                    // });
                    authModel.changeIsLoadingAuth();
                    authModel.sendSmsCode('+996$phoneNumber');
                    /* Navigator.of(context).pushNamed(
                        MainNavigationRouteName.authSms,
                        arguments: phoneNumber);*/
                  }
                },
                child: Text(widget.next,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Gliroy',
                      fontSize: 15,
                    ))),
          ),
        ],
      ),
    );
  }



  Future<String?> validateOtp(String otp) async {
    // await Future.delayed(Duration(milliseconds: 2000));
    // if (otp == "1234") {
    //   return null;
    // } else {
    //   return "The entered Otp is wrong";
    // }
  }

  // action to be performed after OTP validation is success
  void moveToNextScreen(context) {
    //   Navigator.push(context, MaterialPageRoute(
    //       builder: (context) => SuccessfulOtpScreen()));
    // }
  }

  void setIsTimeOut() {
    setState(() {
      isTimeOut = !isTimeOut;
    });
  }
}

class Tic extends StatefulWidget {
  final time = 'У вас осталось';

  const Tic({
    Key? key,
  }) : super(key: key);

  @override
  _TicState createState() => _TicState();
}

class _TicState extends State<Tic> {
  int minute = 0;
  int second = 0;
  int count = 0;
  late Timer timer;
  _AuthorizationScreenState? parentState;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(const Duration(milliseconds: 100), (timer) {
      ticc();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    parentState = context.findAncestorStateOfType<_AuthorizationScreenState>();
    return Align(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('${widget.time}',
              style: const TextStyle(
                  color: Color(0xFFB0B1B2),
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Gliroy')),
          Text(' 0$minute : ${second > 9 ? second : '0$second'}',
              style: const TextStyle(
                  color: Color(0xFFB0B1B2),
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Gliroy')),
        ],
      ),
    );
  }

  void ticc() {
    setState(() {
      count++;
      second = 59 - count % 60;
      if (second == 0 && minute < 0) {
        minute--;
      } else if (second == 0 && minute == 0) {
        parentState?.setIsTimeOut();
        timer.cancel();
      }
    });
  }
}
