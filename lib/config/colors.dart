
import 'package:flutter/material.dart';

const mainBlockColor=Color(0xB3FFCC00);
const redMain=Color(0xFFE3000F);
const background=Color(0xFFf4f5f6);
const backgroundItemMenu=Color(0xB3FFFFFF);
const blackMain=Color(0xFF000000);
const secondaryGray=Color(0xFF828282);
const descriptionLightGray=Color(0xFFC6C7CA);
const descriptionGray=Color(0xFFB0B1B3);
const yellowMain=Color(0xFFFFCC00);
const fabUnselectIconColor=Color(0xFF4E4763);
const addButtonColor=Color(0x4Dc4c4c4);
const disable= Color(0xFFC6C7CA);
const primaryOrange= Color(0xFFE86A12);




var token= "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjMyNjQzNjI1LCJqdGkiOiI3ZjY5MzQ2M2I2ZmQ0OWI5YWZjNzhlMDc3ZDAyMTBhMSIsInVzZXJfaWQiOjMzfQ.qKdXoT2rBLX3JQ-mgZaIg0zV9yy3shJCld0PUFpeSg4";


const textStyleBlack =TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.w800,
        fontFamily: 'Gliroy');

Widget sizedBox(double w,double h)=>SizedBox(width: w,height: h,);



/*
 {
  "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYzMzE2MjAyNSwianRpIjoiMWI5MmVlYmY1N2EyNDU2MDk4YjFkYzY4OWQyMTYxMzgiLCJ1c2VyX2lkIjozM30.8o6m_QxN7Qx3z1gq53pG3vwsRuWSAzGxIAnKMmm1FIQ",
  "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjMyNjQzNjI1LCJqdGkiOiI3ZjY5MzQ2M2I2ZmQ0OWI5YWZjNzhlMDc3ZDAyMTBhMSIsInVzZXJfaWQiOjMzfQ.qKdXoT2rBLX3JQ-mgZaIg0zV9yy3shJCld0PUFpeSg4"
}
*/


