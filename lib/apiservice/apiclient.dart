import 'dart:convert';
import 'dart:io';

import 'package:nypizza/config/constants.dart';
import 'package:nypizza/domain/model/category.dart';
import 'package:nypizza/domain/model/foods.dart';
import 'package:nypizza/domain/model/numberverify.dart';
import 'package:nypizza/domain/model/user.dart';
import 'package:http/http.dart' as http;

enum ApiClientExceptionType { Network, Auth, Other }

class ApiClientException implements Exception {
  final ApiClientExceptionType type;

  ApiClientException(this.type);
}

class ApiClient {
  final client = HttpClient();

  Future<Foods> fetchFoods() async {
    print('foods');
    var json = await getJson('/mobile_app/foods/', requestType: 'get');
    print('foods $json');
    final value = Foods.fromJson(json);
    return value;
  }

  Future<List<Category>> getCategory() async {
    print('c ');
    var json =
        await getJson('/mobile_app/foods/categories/', requestType: 'get')
            as List<dynamic>;
    return json.map((e) {
      return Category.fromJson(e);
    }).toList();
  }

  Future<Map<String, dynamic>?> fetchHomeContent() async {
    final foods = await fetchFoods();
    final category = await getCategory();
    print('$foods,$category');
    if (foods != null && category != null) {
      return {'foods': foods, 'category': category};
    }
  }

  Future<String?> sendSmsCode(String phoneNumber) async {
    var body = <String, dynamic>{};
    body['phone_number'] = phoneNumber;
    var json =
        await getJson('/mobile_app/accounts/token/send-sms/', body: body);
    return json;
  }

  Future<NumberVerify?> verify(String phoneNumber, String code) async {
    var body = <String, dynamic>{};
    var body2 = <String, dynamic>{};
    body['phone_number'] = phoneNumber;
    body['activation_code'] = code;
    var json =
        await getJson('/mobile_app/accounts/token/number/verify/', body: body);
    if (json == '') {
      body2['phone_number'] = phoneNumber;
      var json2 = await getJson('/mobile_app/accounts/token/', body: body2);
      return NumberVerify.fromJson(json2);
    } else {
      throw const SocketException('Пользоватедь не найден');
    }
  }

  Future<UserModelProvider?> getUserProfile(String token) async {
/*
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var json = await getJson('/mobile_app/accounts/profile',
        requestType: 'get', headers: headers);
*/
    // try {
    //   return UserModelProvider.fromJson(json);
    // } catch (e) {
    //   return throw Exception(json);
    // }

    final response = await http.get(
      _makeUri('/mobile_app/accounts/profile'),
      // Send authorization headers to the backend.
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    final responseJson = jsonDecode(response.body);
    print(responseJson);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return UserModelProvider.fromJson(responseJson);
    } else {
      return Future.error(responseJson);
    }
  }

  Future<Map<String, dynamic>?> fetchAppContent(String token) async {
    try {
      final home = await fetchFoods();
      final profile = await getUserProfile(token);
      return {"home": home, "profile": profile};
    } catch (e) {
      return throw Exception('Error');
    }
  }

  Future<dynamic> getJson(String path,
      {Map<String, dynamic>? parameters,
      Map<String, dynamic>? body,
      String? requestType,
      Map<String, dynamic>? headers}) async {
    final url = _makeUri(path, parameters);
    final HttpClientRequest request;

    if (requestType == null) {
      request = await client.postUrl(url);
    } else {
      request = await client.getUrl(url);
    }

    request.headers.contentType =ContentType.json;
    if (headers != null) {

      request.headers.set('headers', headers);
    }
    if (body != null) {
      request.write(jsonEncode(body));
    }
    final HttpClientResponse response;

    response = await request.close();
    var json = await response.jsonDecode();
    print('json: $json');
    print('response status code: ${response.statusCode}');
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return json;
    } else {
      return Future.error(Exception(json));
    }
  }

  Uri _makeUri(String path, [Map<String, dynamic>? parameters]) {
    final uri = Uri.parse('$baseUrl$path');

    if (parameters != null) {
      return uri.replace(queryParameters: parameters);
    } else {
      return uri;
    }
  }
}

extension HttpClientResponseJsonDecode on HttpClientResponse {
  dynamic jsonDecode() {
    return transform(utf8.decoder)
        .toList()
        .then((value) => value.join())
        .then((value) {
      if (value.isNotEmpty) {
        return json.decode(value);
      } else {
        return value;
      }
    });
  }
}
