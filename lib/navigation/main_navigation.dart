import 'package:flutter/material.dart';
import 'package:nypizza/ui/auth/authorization.dart';
import 'package:nypizza/ui/auth/authorizationsms.dart';
import 'package:nypizza/ui/example/ex.dart';
import 'package:nypizza/ui/splash/splash.dart';

import '../app.dart';

abstract class MainNavigationRouteName {
  static const splash = 'splash';
  static const auth = '/auth';
  static const authSms = '/auth/sms';
  static const app = 'app';
}

class MainNavigation {
  final initialRoute = MainNavigationRouteName.splash;
  final routes = <String, Widget Function(BuildContext)>{
    MainNavigationRouteName.splash: (context) => const  MySplashScreen(),
    MainNavigationRouteName.auth: (context) =>  const AuthScreen(),
  };

   Route<Object> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case MainNavigationRouteName.app:
        return MaterialPageRoute(
            builder: (context) =>
                InitApp(phoneNumber: settings.arguments as String?));
      case MainNavigationRouteName.authSms:
        return MaterialPageRoute(
            builder: (context) =>
                AuthSmsScreen(phoneNumber: settings.arguments as String)
        );

      default:
        return MaterialPageRoute(
            builder: (context) => const Text('Navigation Error'));
    }
  }
}
