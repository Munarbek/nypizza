import 'package:flutter/material.dart';
import 'package:nypizza/config/colors.dart';

class CircleImage extends StatelessWidget {
  const CircleImage({
    Key? key,
    required this.imageProvider,
    this.imageRadius = 20,
  }) : super(key: key);

  final double imageRadius;
  final ImageProvider imageProvider;

@override
Widget build(BuildContext context) {
// 3
  return CircleAvatar(
    backgroundColor: redMain,
    radius: imageRadius,
// 4
    child: CircleAvatar(
      radius: imageRadius-1,
      backgroundImage: imageProvider,
    ),
  );
}
}

