import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:nypizza/apiservice/apiclient.dart';
import 'package:nypizza/domain/data/securitystorage/security_storage.dart';
import 'package:nypizza/domain/model/myappmodel.dart';
import 'package:nypizza/domain/model/user.dart';
import 'package:nypizza/domain/respository/repository.dart';
import 'package:nypizza/navigation/main_navigation.dart';

class AppInherited extends InheritedWidget{
  final mainNavigation=MainNavigation();
  final repository = Repository(ApiClient());
  final securityStorage = SecurityStorage();
  final myAppModel = MyAppModel();

  final Box<Map<String,dynamic>>? userBox;
   AppInherited({Key? key, Widget? child,this.userBox}) : super(key: key,child: child!);

  static AppInherited of(BuildContext context){
    return context.getElementForInheritedWidgetOfExactType<AppInherited>()?.widget as AppInherited;
  }
  @override
  bool updateShouldNotify(covariant AppInherited oldWidget) {
    return oldWidget.child!=child;
  }
}