import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nypizza/config/colors.dart';
import 'package:rive/rive.dart';

class ProgressWidget extends StatelessWidget {
  const ProgressWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: const [
          SizedBox(height: 150, width: 150, child: Avatar()),
          SizedBox(
              width: 150,
              child: LinearProgressIndicator(
                backgroundColor: Colors.white,
                color: redMain,
              ))
        ],
      ),
    );
  }
}

class Avatar extends StatefulWidget {
  const Avatar({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _Avatar();
}

class _Avatar extends State<Avatar> with TickerProviderStateMixin {
  AnimationController? _controller;
  final Tween<double> _tween = Tween(begin: 0.75, end: 1.3);

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
        duration: const Duration(milliseconds: 900), vsync: this);
    _controller?.repeat(reverse: true);
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Align(
          child: ScaleTransition(
            scale: _tween.animate(
                CurvedAnimation(parent: _controller!, curve: Curves.easeInOut)),
            child: SizedBox(
                height: 100,
                width: 100,
                child: SvgPicture.asset('assets/images/Logo.svg')),
          ),
        ),
      ],
    );
  }
}

class RiveAnimationPage extends StatelessWidget {
  const RiveAnimationPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(10.0),
      child: SizedBox(
        width: 200,
        height: 200,
        child:  RiveAnimation.asset("assets/anim/new.riv",
        ),
      ),
    );
  }
}
