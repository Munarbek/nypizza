import 'package:flutter/material.dart';
import 'package:nypizza/domain/model/category.dart';

class FoodCategoriesItem extends StatelessWidget {
  BoxDecoration? boxdecoration;

  final Category _category;

  FoodCategoriesItem(this._category, {Key? key, this.boxdecoration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
              width: 64,
              height: 64,
              child: _category.cover_file==null ? Image.asset('assets/images/imagePizzaMenu.png') : Image.network('${_category.cover_file}') ),
          const SizedBox(
            height: 12,
          ),
          Text(
            'Пицца',
            style: const TextStyle(
              fontWeight: FontWeight.w700,
              fontFamily: 'Gliroy',
              fontSize: 14,
            ),
          )
        ],
      ),
    );
  }
}
