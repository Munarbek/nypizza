
import 'package:flutter/material.dart';
import 'package:like_button/like_button.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/domain/model/food.dart';
import 'package:nypizza/domain/model/pizza.dart';
import 'package:nypizza/domain/model/user.dart';

import 'package:provider/provider.dart';

class ItemPizzaHome extends StatefulWidget {
  late Food? pizza;

  ItemPizzaHome({Key? key, required this.pizza}) : super(key: key);

  @override
  State<ItemPizzaHome> createState() => _ItemPizzaHomeState();
}

class _ItemPizzaHomeState extends State<ItemPizzaHome> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    User user = Provider.of<User>(context);
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        height: 120,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(13),
        ),
        child: Row(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(13.0),
                child: widget.pizza?.image!=null ? Image.network('${widget.pizza?.image}'):Image.asset('assets/images/iconspizza2.png')),
            Expanded(
                child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                              '${widget.pizza?.name} (${widget.pizza?.gram} гр)',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis ,
                              style: const TextStyle(
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Gliroy',
                                fontSize: 16,
                              )),
                        ),

                        LikeButton(
                        circleColor: const CircleColor(start: redMain, end: yellowMain),
                        bubblesColor: const BubblesColor(
                          dotPrimaryColor: yellowMain,
                          dotSecondaryColor: redMain,
                        ),
                        likeBuilder: (isLiked) {
                          print(isLiked);
                          return Icon(
                            isLiked ? Icons.favorite_rounded : Icons.favorite_border_rounded,
                            color: redMain,

                          );
                        },
                            )
                      ]),
                  const SizedBox(height: 4),
                  Text('${widget.pizza?.desc}',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontFamily: 'Gliroy',
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      )),
                  const SizedBox(height: 4),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('${widget.pizza?.price} сом',
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Gliroy',
                              fontSize: 16,
                            )),
                        Row(
                          children: [
                            Material(
                              borderRadius: BorderRadius.circular(10),
                              color: addButtonColor,
                              child: InkWell(
                                splashColor: addButtonColor,
                                borderRadius: BorderRadius.circular(10),
                                child: Container(
                                  height: 38,
                                  width: 38,
                                  child: const Center(
                                    child: Text(
                                      '-',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 22),
                                    ),
                                  ),
                                ),
                                onTap: () {

                                },
                              ),
                            ),
                            Text(' 10  ',
                                style: const TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'Gliroy',
                                  fontSize: 16,
                                )),
                            Material(
                              borderRadius: BorderRadius.circular(10),
                              color: addButtonColor,
                              child: InkWell(
                                splashColor: addButtonColor,
                                borderRadius: BorderRadius.circular(10),
                                child: const SizedBox(
                                  height: 38,
                                  width: 38,
                                  child: Center(
                                    child: Text(
                                      '+',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 20),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  // widget.pizza.countAddToCard++;
                                  // user.addToBasked(widget.pizza);
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }
}

/*
* Material(
      borderRadius: BorderRadius.circular(20),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        splashColor: Theme.of(context).primaryColorLight,
        child: Container(
          height: 100,
          width: 100,
        ),
        onTap: () {},
      ),
    );
*
* */
