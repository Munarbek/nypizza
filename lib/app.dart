import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:nypizza/config/colors.dart';
import 'package:nypizza/domain/model/foodsmodel.dart';
import 'package:nypizza/navigation/main_navigation.dart';
import 'package:nypizza/presentation/icons.dart';
import 'package:nypizza/ui/auth/provider/authmodelprovider.dart';
import 'package:nypizza/ui/basked/basked.dart';
import 'package:nypizza/ui/basked/ordering/ordering.dart';
import 'package:nypizza/ui/home/home.dart';
import 'package:nypizza/ui/orderhistory/order_history.dart';
import 'package:nypizza/ui/qr/qr.dart';
import 'package:nypizza/ui/user/userscreen.dart';
import 'package:nypizza/widgets/inherited/appinherited.dart';
import 'package:nypizza/widgets/itemfader.dart';
import 'package:nypizza/widgets/progresswidget.dart';
import 'package:provider/provider.dart';
import 'config/constants.dart';
import 'domain/model/pizza.dart';
import 'domain/model/user.dart';

class InitApp extends StatefulWidget {
  final String? phoneNumber;

  const InitApp({Key? key, required this.phoneNumber}) : super(key: key);

  @override
  State<InitApp> createState() => _InitAppState();
}

class _InitAppState extends State<InitApp> {
  late User _user;

  @override
  Widget build(BuildContext context) {
    print('Build InitApp');
    final _repository = AppInherited.of(context).repository;
    final _storage = AppInherited.of(context).myAppModel;
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AuthModelProvider>(
              create: (_) => AuthModelProvider(_repository)),
          ChangeNotifierProvider<FoodsModel>(
              create: (_) => FoodsModel(_repository)),
          ChangeNotifierProvider<User>(
              create: (_) => User(_repository, _storage)),
        ],
        child: Consumer<User>(builder: (context, userProvider, child) {
          _user = userProvider;
          return ItemFader(
            child: widget.phoneNumber != null
                ? FutureBuilder<UserModelProvider?>(
                    future: initUser(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data != null) {
                        userProvider.userModelProvider = snapshot.data!;
                        userProvider.userBox
                            .put(boxUserKey, userProvider.userModelProvider!);
                        //print(userProvider.userBox.values.first);
                        return App(phoneNumber: widget.phoneNumber);
                      } else if (snapshot.hasError) {
                        return Scaffold(
                            body: Center(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Text(errorMessage),
                                IconButton(
                                  icon: Image.asset('assets/images/reset.png'),
                                  iconSize: 50,
                                  onPressed: () {
                                    setState(() {});
                                  },
                                )
                              ]),
                        ));
                      } else {
                        return const Scaffold(
                            body: Center(child: RiveAnimationPage()));
                      }
                    },
                  )
                : const App(phoneNumber: null),
          );
        }));
  }

  Future<UserModelProvider?> initUser() async {
    _user.userBox = Hive.box<UserModelProvider>(boxName);
    return _user.getUserProfile();
  }
}

class App extends StatefulWidget {
  final String? phoneNumber;

  const App({Key? key, required this.phoneNumber}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<App> {
  late List<Pizza> listPizza;
  int _selectedIndex = 0;
  bool _fabIsSelected = false;
  late List<dynamic> pages;
  late User _user;
  FoodsModel? _foodsModel;

  @override
  void initState() {
    listPizza = Pizza.listPizzas();
    pages = [
      Home(),
      const OrderingScreen(),
      BaskedScreen(),
      const OrderHistoryScreen(),
      const UserScreen()
    ];
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _fabIsSelected = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: mainBlockColor,
      ),
    );
    print('Build App');

    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;
    return Scaffold(
      body: SafeArea(child: Consumer2<User, FoodsModel>(
        builder: (context, userProvider, foodsModel, child) {
          var boxUser = userProvider.userModelProvider;
          _foodsModel = foodsModel;
          return widget.phoneNumber != null
              ? Container(
                  color: background,
                  child: Column(
                    children: [
                      Container(
                        height: 110,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(25.0),
                              bottomRight: Radius.circular(25.0)),
                          color: mainBlockColor,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    width: 58,
                                    height: 58,
                                    decoration: BoxDecoration(
                                        color: redMain,
                                        borderRadius:
                                            BorderRadius.circular(30.0)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: CircleAvatar(
                                        backgroundColor: background,
                                        backgroundImage: boxUser?.avatar != null
                                            ? NetworkImage('${boxUser?.avatar}')
                                            : Image.asset(
                                                    'assets/images/iconsuser.png')
                                                .image,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Container(
                                    constraints:
                                        const BoxConstraints(maxWidth: 70),
                                    child: Text(
                                      boxUser?.firstName != ""
                                          ? '${boxUser?.firstName}'
                                          : '0${boxUser?.phoneNumber?.split('+996')[1]}',
                                      style: const TextStyle(
                                          overflow: TextOverflow.ellipsis,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  const SizedBox(
                                    height: 18,
                                    width: 18,
                                    child: Image(
                                        image: AssetImage(
                                            'assets/images/image1343.png')),
                                  ),
                                  Text(
                                    '${boxUser?.bonus}',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20,
                                    ),
                                  ),
                                  const Text(
                                    'Бонусы',
                                    style: TextStyle(),
                                  ),
                                ],
                              ),
                              const Image(
                                  image: AssetImage('assets/images/line.png')),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  SizedBox(
                                      width: 18,
                                      height: 18,
                                      child: Image.asset(
                                          'assets/images/imaged.png')),
                                  Text(
                                    '${boxUser?.status?.discount}%',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20,
                                    ),
                                  ),
                                  Text(
                                    '${boxUser?.status?.desc?.split(' ')[0]}',
                                    style: const TextStyle(),
                                  ),
                                ],
                              ),
                              SvgPicture.asset('assets/images/Logo.svg')
                            ],
                          ),
                        ),
                      ),
                      _fabIsSelected
                          ? const QrScreen()
                          : Expanded(
                              child: pages[_selectedIndex],
                            ),
                    ],
                  ),
                )
              : Container(
                  height: 110,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0)),
                    color: mainBlockColor,
                  ),
                  child:
                      Center(child: SvgPicture.asset('assets/images/Logo.svg')),
                );
        },
      )),
      bottomNavigationBar: Theme(
        data: ThemeData(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          items: [
            const BottomNavigationBarItem(
              icon: Icon(MyFlutterApp.group),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Badge(
                elevation: 5,
                animationDuration: const Duration(milliseconds: 20),
                borderSide: const BorderSide(width: 1, color: Colors.white),
                badgeColor: const Color(0xFFFD5C2E),
                animationType: BadgeAnimationType.scale,
                position: const BadgePosition(start: 20, bottom: 13),
                badgeContent: const Text('10',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gliroy',
                      color: Colors.white,
                      fontSize: 15,
                    )),
                child: const Icon(MyFlutterApp.shopping_basket_1),
              ),
              label: 'Business',
            ),
            const BottomNavigationBarItem(
              icon: SizedBox(
                width: 40,
                height: 10,
              ),
              label: 'School',
            ),
            const BottomNavigationBarItem(
              icon: Icon(MyFlutterApp.shopping_list_1),
              label: 'School',
            ),
            const BottomNavigationBarItem(
              icon: Icon(MyFlutterApp.user_1),
              label: 'School',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: redMain,
          onTap: (index) {
            if (_selectedIndex != index) {
              if (widget.phoneNumber != null) {
                if (index == 2) {
                  return;
                } else {
                  _onItemTapped(index);
                }
              } else {
                showMyDialog(context);
              }
            }
          },
        ),
      ),
      floatingActionButton: showFab
          ? FloatingActionButton(
              child: Icon(MyFlutterApp.qr_code_1,
                  color: _fabIsSelected ? redMain : fabUnselectIconColor),
              backgroundColor: yellowMain,
              onPressed: () {
                if (widget.phoneNumber != null) {
                  setState(() {
                    if (!_fabIsSelected) {
                      _fabIsSelected = !_fabIsSelected;
                    }
                    _selectedIndex = 2;
                  });
                } else {
                  showMyDialog(context);
                }
              },
            )
          : null,
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
    );
  }

  showMyDialog(BuildContext context) => showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          backgroundColor: background,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  child: const Icon(Icons.close_outlined),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              SvgPicture.asset('assets/images/signinpage.svg'),
              const SizedBox(
                height: 16,
              ),
              const Text(
                'Для того чтобы выполнить данное действие, вам необходимо авторизоваться.',
                textAlign: TextAlign.center,
                style: TextStyle(fontFamily: 'Gliroy'),
              ),
              const SizedBox(
                height: 8,
              ),
              SizedBox(
                width: double.infinity,
                height: 45,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: redMain,
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30), // <-- Radius
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          MainNavigationRouteName.auth, (route) => false);
                    },
                    child: const Text('Пройти авторизацию',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Gliroy',
                          fontSize: 15,
                        ))),
              ),
            ],
          ),
        ),
      );
}

/*
 ? FutureBuilder<Box<Map<String, dynamic>>?>(
                      future: Hive.openBox<Map<String, dynamic>>(boxName),
                      builder: (context, boxSnapshot) {
                        if (boxSnapshot.hasData) {
                          var box = boxSnapshot.data;
                          if (box?.get(boxUserKey) != null) {
                            var boxUser = UserModelProvider.fromJson(
                                box?.get(boxUserKey));
                            return Container(
                              color: background,
                              child: Column(
                                children: [
                                  Container(
                                    height: 110,
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(25.0),
                                          bottomRight: Radius.circular(25.0)),
                                      color: mainBlockColor,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(16.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                width: 58,
                                                height: 58,
                                                decoration: BoxDecoration(
                                                    color: redMain,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30.0)),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(4.0),
                                                  child: CircleAvatar(
                                                    backgroundColor: background,
                                                    backgroundImage: boxUser.avatar !=
                                                            null
                                                        ? NetworkImage(
                                                            '${boxUser.avatar}')
                                                        : Image.asset(
                                                                'assets/images/iconsuser.png')
                                                            .image,
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 4,
                                              ),
                                              Container(
                                                constraints:
                                                    const BoxConstraints(
                                                        maxWidth: 70),
                                                child: Text(
                                                  boxUser.firstName ??
                                                      '0${boxUser.phoneNumber?.split('+996')[1]}',
                                                  style: const TextStyle(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              const SizedBox(
                                                height: 18,
                                                width: 18,
                                                child: Image(
                                                    image: AssetImage(
                                                        'assets/images/image1343.png')),
                                              ),
                                              Text(
                                                '${boxUser.bonus}',
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              const Text(
                                                'Бонусы',
                                                style: TextStyle(),
                                              ),
                                            ],
                                          ),
                                          const Image(
                                              image: AssetImage(
                                                  'assets/images/line.png')),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              SizedBox(
                                                  width: 18,
                                                  height: 18,
                                                  child: Image.asset(
                                                      'assets/images/imaged.png')),
                                              Text(
                                                '${boxUser.status?.discount}%',
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Text(
                                                '${boxUser.status?.desc?.split(' ')[0]}',
                                                style: const TextStyle(),
                                              ),
                                            ],
                                          ),
                                          SvgPicture.asset(
                                              'assets/images/Logo.svg')
                                        ],
                                      ),
                                    ),
                                  ),
                                  _fabIsSelected
                                      ? const QrScreen()
                                      : Expanded(
                                          child: pages[_selectedIndex],
                                        ),
                                ],
                              ),
                            );
                          } else {
                            return FutureBuilder<UserModelProvider?>(
                              future: userProvider.getUserProfile(),
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  var user = snapshot.data;
                                  _user.userModelProvider =
                                      user as UserModelProvider;
                                  box?.put(boxUserKey, user.toJson());
                                  return Container(
                                    color: background,
                                    child: Column(
                                      children: [
                                        Container(
                                          height: 110,
                                          decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                bottomLeft:
                                                    Radius.circular(25.0),
                                                bottomRight:
                                                    Radius.circular(25.0)),
                                            color: mainBlockColor,
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Column(
                                                  children: [
                                                    Container(
                                                      width: 58,
                                                      height: 58,
                                                      decoration: BoxDecoration(
                                                          color: redMain,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      30.0)),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(4.0),
                                                        child: CircleAvatar(
                                                          backgroundColor:
                                                              background,
                                                          backgroundImage: user
                                                                      .avatar !=
                                                                  null
                                                              ? NetworkImage(
                                                                  '${user.avatar}')
                                                              : Image.asset(
                                                                      'assets/images/iconsuser.png')
                                                                  .image,
                                                        ),
                                                      ),
                                                    ),
                                                    const SizedBox(
                                                      height: 4,
                                                    ),
                                                    Container(
                                                      constraints:
                                                          const BoxConstraints(
                                                              maxWidth: 70),
                                                      child: Text(
                                                        user.firstName ??
                                                            '0${user.phoneNumber?.split('+996')[1]}',
                                                        style: const TextStyle(
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: [
                                                    const SizedBox(
                                                      height: 18,
                                                      width: 18,
                                                      child: Image(
                                                          image: AssetImage(
                                                              'assets/images/image1343.png')),
                                                    ),
                                                    Text(
                                                      '${user.bonus}',
                                                      style: const TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    const Text(
                                                      'Бонусы',
                                                      style: TextStyle(),
                                                    ),
                                                  ],
                                                ),
                                                const Image(
                                                    image: AssetImage(
                                                        'assets/images/line.png')),
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: [
                                                    SizedBox(
                                                        width: 18,
                                                        height: 18,
                                                        child: Image.asset(
                                                            'assets/images/imaged.png')),
                                                    Text(
                                                      '${user.status?.discount}%',
                                                      style: const TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    Text(
                                                      '${user.status?.desc?.split(' ')[0]}',
                                                      style: const TextStyle(),
                                                    ),
                                                  ],
                                                ),
                                                SvgPicture.asset(
                                                    'assets/images/Logo.svg')
                                              ],
                                            ),
                                          ),
                                        ),
                                        _fabIsSelected
                                            ? const QrScreen()
                                            : Expanded(
                                                child: pages[_selectedIndex],
                                              ),
                                      ],
                                    ),
                                  );
                                } else if (snapshot.hasError) {
                                  return Center(
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          const Text(
                                            errorMessage,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Gliroy'),
                                          ),
                                          const SizedBox(height: 8),
                                          IconButton(
                                            icon: Image.asset(
                                                'assets/images/reset.png'),
                                            iconSize: 50,
                                            onPressed: () {
                                              setState(() {});
                                            },
                                          )
                                        ]),
                                  );
                                }
                                return const Center(child: RiveAnimationPage());
                              },
                            );
                          }
                        } else if (boxSnapshot.hasError) {
                          return Center(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Text(
                                    errorMessage,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Gliroy'),
                                  ),
                                  const SizedBox(height: 8),
                                  IconButton(
                                    icon:
                                        Image.asset('assets/images/reset.png'),
                                    iconSize: 50,
                                    onPressed: () {
                                      setState(() {});
                                    },
                                  )
                                ]),
                          );
                        }
                        return const Center(child: RiveAnimationPage());
                      },
                    )
 */
